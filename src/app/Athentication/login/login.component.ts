import { Component, OnInit } from '@angular/core';
import { TimesheetService } from 'src/app/Layout/Services/timesheet.service';
import { ILogin } from 'src/app/Layout/Models/timesheet.model';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public LoginModel: ILogin = <ILogin>{};
  public addSubscription: Subscription;
  public loggedin: boolean = true;
  public IsSignIn: boolean = false;
  constructor(public router: Router, private timesheetService: TimesheetService) { }

  loginpage(model) {    
    this.addSubscription = this.timesheetService.login(model, 'Login').subscribe((response) => {
      if (response && response["successful"]) {
        if (response['data'][0].UserType == 'Admin' || response['data'][0].UserType == 'Employee') {
          this.loggedin = true;
          this.IsSignIn = true;
          localStorage.setItem('currentUser', JSON.stringify(response['data']));
          this.router.navigate(['/timesheet']);
        } else {
          this.loggedin = false;
        }
      } else {

      }
    });
  }
}
