import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { LayoutModule } from '@angular/cdk/layout';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { TabViewModule } from 'primeng/tabview';
import { NgxPaginationModule } from 'ngx-pagination';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { NgxSpinnerModule } from 'ngx-spinner';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { MultiSelectModule } from 'primeng/multiselect';
import { ChipsModule } from 'primeng/chips';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CKEditorModule } from 'ngx-ckeditor';
import { LoginComponent } from './Athentication/login/login.component';
import { LoginRoutingModule } from './Athentication/login/login-routing.module';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { SharedComponentsModule } from 'src/app/Layout/Components/SharedComponents/shared-components/shared-components.module';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    LayoutModule,
    MDBBootstrapModule.forRoot(),
    BrowserAnimationsModule,
    TabViewModule,
    NgxPaginationModule,
     SharedComponentsModule,
    PdfViewerModule,
    NgxSpinnerModule,
    Ng2SearchPipeModule,
    MultiSelectModule,
    ChipsModule,
    InputSwitchModule,
    CKEditorModule,
    LoginRoutingModule,
    NgxDaterangepickerMd.forRoot(),
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
