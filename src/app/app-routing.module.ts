import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'login', loadChildren: './Athentication/login/login.module#LoginModule' },
  { path: 'timesheet', loadChildren: './Layout/Components/timesheet/timesheet.module#TimesheetModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
