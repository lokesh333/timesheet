import { Component, OnChanges, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import * as moment from 'moment';
@Component({
  selector: 'app-date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['./date-range-picker.component.scss']
})
export class DateRangePickerComponent implements OnChanges {
  // selected: { startDate: Moment, endDate: Moment };
  @Output() dateRangeEvent = new EventEmitter<Range>();
  @Input() maxDate: object;
  @Input() minDate: object;
  @Input() callMethod: any;
  public selected: any;
  public alwaysShowCalendars: boolean;
  public ranges: any = {
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(7, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  }
  // invalidDates: moment.Moment[] = [moment().add(2, 'days'), moment().add(3, 'days'), moment().add(5, 'days')];
  // isInvalidDate = (m: moment.Moment) => {
  //   return this.invalidDates.some(d => d.isSame(m, 'day'))
  // }
  constructor() {
    this.alwaysShowCalendars = true;
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.callMethod && this.callMethod && this.callMethod.startDate && this.callMethod.endDate) {
      this.updateRange(this.callMethod);
    }
  }
  updateRange(range: Range) {
    if (range) {
      this.selected = range;
      this.dateRangeEvent.emit(this.selected)
    }
  }

}
