import { Component, OnInit, ViewChild, HostListener, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IRequirement } from 'src/app/Layout/Models/timesheet.model';
import { Subscription } from 'rxjs';
import { TimesheetService } from 'src/app/Layout/Services/timesheet.service';
import { ModalDirective } from 'angular-bootstrap-md';
import { DatePipe } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-requiremnts',
  templateUrl: './requiremnts.component.html',
  styleUrls: ['./requiremnts.component.scss']
})
export class RequiremntsComponent implements OnInit {
  @ViewChild('AddForm', { static: false }) public AddForm: any;
  @ViewChild('PopUpModal', { static: false }) public PopUpModal: ModalDirective;
  @ViewChild('DetailModal', { static: false }) public DetailModal: ModalDirective;
  @ViewChild('modalConfirmDelete', { static: false }) public modalConfirmDelete: ModalDirective;
  public loggedInuserDetails: any = [];
  public AddRequirement: boolean = false;
  public RequirementModel: IRequirement = <IRequirement>{};
  public searchRequirementsSubscription: Subscription;
  public addRequirementsSubscription: Subscription;
  public getRequirements: any;
  public getProjects: any;
  public getPhases: any;
  public getEmployees: any;
  public PhaseList: any;
  public RequirementList: any;
  public unsuccessmsg: string = "";
  public successmsg: string = "";
  public Title: string = "";
  public ReqDetail: string = "";
  public addDetail: boolean;
  public popup: boolean;
  public link: any;
  public getRequirementsFixed: any;
  public FilteredTypes: Set<unknown>;
  public FilteredProjects: Set<unknown>;
  public FilteredFrom: Set<unknown>;
  public FilteredPhases: Set<unknown>;
  public FilteredTo: Set<unknown>;
  public FilteredPriority: Set<unknown>;
  public FilteredStatus: Set<unknown>;
  public EnterRequirement: boolean = false;
  public getTasks: any;
  public aa: number;
  public getReqDescTable: any;
  public isChecked: any = false;
  public addReqDetail: boolean = false;
  public RequirementID: number;
  public CheckBox: Event;
  public ToSkype: any;
  constructor(private timesheetService: TimesheetService,private _snackBar: MatSnackBar,
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.loggedInuserDetails = JSON.parse(localStorage.getItem('currentUser'));
    this.loggedInuserDetails = this.loggedInuserDetails[0];
    this.onInitRequirements('REQUIREMENTS');
    this.onInitRequirements('ProjectSelectActive');
    this.onInitRequirements('PhaseSelectActive');
    this.onInitRequirements('TaskSelectActive');
    this.onInitRequirements('AllEmployees');
    this.RequirementModel.From = this.loggedInuserDetails.EmployeeName;
  }

  callRequirementMethods() {
    this.onInitRequirements('REQUIREMENTS');
    this.onInitRequirements('ProjectSelectActive');
    this.onInitRequirements('PhaseSelectActive');
    this.onInitRequirements('AllEmployees');
  }

  addRequirement() {
    this.AddRequirement = true;
    this.RequirementModel.Status = 'New';
    this.RequirementModel.From = this.loggedInuserDetails.EmployeeName;
    this.RequirementModel.RequirementName = null;
    this.EnterRequirement = false;
    this.RequirementModel.ProjectName = null;
    this.RequirementModel.PhaseName = null;
  }
  deleteRow() {
    this.AddRequirement = false;
  }
  saveRequirement(model, input) {
    this.modalConfirmDelete.hide();
    this.addRequirementsSubscription = this.timesheetService.saveRequirement(model, input).subscribe((response) => {
      if (response && response["successful"]) {
        this.successmsg = response['data'][0].resp;
        if (this.successmsg=='Requirement Already Exist') {
          this._snackBar.open(this.successmsg, 'Close', { duration: 2000 });
        }
        switch (input) {
          case 'REQUIREMENTS_Insert':
            this.onInitRequirements('REQUIREMENTS');
            this.AddForm.resetForm();
            break;
          case 'REQUIREMENTS_UPDATE':
            this.onInitRequirements('REQUIREMENTS');
            this.AddForm.resetForm();
            break;
          case 'REQUIREMENTS_DELETE':
            this.onInitRequirements('REQUIREMENTS');
            this.AddForm.resetForm();
            break;
          case 'ReqDescInsert':
            this.onInitRequirementsDesc('ReqDescSelect', this.RequirementModel.Id, this.RequirementModel);
            this.RequirementModel.RequirementDetail = null;
            break;
          case 'ReqDescDelete':
            this.RequirementModel.Id = this.RequirementID;
            this.onInitRequirementsDesc('ReqDescSelect', this.RequirementModel.Id, this.RequirementModel);
            break;
          case 'ReqDescUpdateCheck':
            this.RequirementModel.Id = this.RequirementID;
            this.onInitRequirementsDesc('ReqDescSelect', this.RequirementModel.Id, null);
            this.AddForm.resetForm();
            break;
          case 'ReqDescUpdateUnCheck':
            this.RequirementModel.Id = this.RequirementID;
            this.onInitRequirementsDesc('ReqDescSelect', this.RequirementModel.Id, null);
            this.AddForm.resetForm();
            break;
        }
      } else {
        switch (input) {
          case 'REQUIREMENTS_UPDATE':
            this.unsuccessmsg = "UnSuccessful";
            break;
        }
      }
    });
    this.AddRequirement = false;
    this.EnterRequirement = false;
  }

  onInitRequirements(input) {
    this.searchRequirementsSubscription = this.timesheetService.onInitRequirements(input, null).subscribe((response) => {
      if (response && response["successful"]) {
        switch (input) {
          case 'REQUIREMENTS':
            this.getRequirements = response['data'];
            this.getRequirementsFixed = response['data'];
            this.FilteredTypes = new Set(response["data"].map((e) => { return e.Type }));
            this.FilteredProjects = new Set(response["data"].map((e) => { return e.ProjectName }));
            this.FilteredPhases = new Set(response["data"].map((e) => { return e.PhaseName }));
            this.FilteredFrom = new Set(response["data"].map((e) => { return e.From }));
            this.FilteredTo = new Set(response["data"].map((e) => { return e.To }));
            this.FilteredPriority = new Set(response["data"].map((e) => { return e.Priority }));
            this.FilteredStatus = new Set(response["data"].map((e) => { return e.Status }));

            break;
          case 'ProjectSelectActive':
            this.getProjects = response["data"];

            break;
          case 'PhaseSelectActive':
            this.getPhases = response["data"];

            break;

          case 'TaskSelectActive':
            this.getTasks = response["data"];

            break;

          case 'AllEmployees':
            this.getEmployees = response["data"];

            break;

        }


      } else {
      }
    });
  }



  PriorityMenu(data) {
    this.RequirementModel.Priority = data;
    this.RequirementModel.Status = 'New';
  }
  TypeMenu(data) {
    this.RequirementModel.Type = data;
  }

  filterEmployee1(data) {
    this.RequirementModel.From = data;
  }

  filterEmployee2(data) {
    this.RequirementModel.To = data;
  }



  filterProject(model) {
    this.RequirementModel.ProjectId = model.ProjectId;
    this.RequirementModel.ProjectName = model.ProjectName;
    model.ProjectId ? this.PhaseList = this.getPhases.filter(x => x['ProjectId'] === model.ProjectId) : this.PhaseList = [];
    this.RequirementModel.PhaseId = null;
    this.RequirementModel.PhaseName = null;
    this.EnterRequirement = false;
  }

  filterPhase(model) {
    this.RequirementModel.PhaseId = model.PhaseId;
    this.RequirementModel.PhaseName = model.PhaseName;
    model.PhaseId ? this.RequirementList = this.getTasks.filter(x => x['PhaseId'] === model.PhaseId) : this.RequirementList = [];
    this.RequirementModel.TaskName = null;
    this.RequirementModel.TaskId = null;
    this.EnterRequirement = false;
  }

  filterRequirement(data) {
    if (data == 'Not Exist') {
      this.EnterRequirement = true;
    }
  }

  //**********show popup**********//
  createPopup = (model, input) => {
    if (input == "Update Requirement") {
      this.RequirementModel.ProjectName = model.ProjectName;
      this.RequirementModel.ProjectId = model.ProjectId;
      this.RequirementModel.PhaseName = model.PhaseName;
      this.RequirementModel.PhaseId = model.PhaseId;
      this.RequirementModel.Type = model.Type;
      this.RequirementModel.RequirementName = model.RequirementName;
      this.RequirementModel.RequirementDetail = model.RequirementDetail;
      this.RequirementModel.From = model.From;
      this.RequirementModel.To = model.To;
      this.RequirementModel.Priority = model.Priority;
      this.RequirementModel.Effort = model.Effort;
      this.RequirementModel.Status = model.Status;
      this.RequirementModel.Id = model.Id;
      this.RequirementModel.TaskId = model.TaskId;
      this.Title = input;
    }

    this.PopUpModal.show();
  }

  addTo = (Model, input) => {
    switch (input) {
      case 'Added to TS':
        Model.TaskName = Model.RequirementName;
        Model.Status = 'Active';
        this.addRequirementsSubscription = this.timesheetService.add(null, Model, "TaskInsertGETID", null).subscribe((response) => {
          if (response && response["successful"]) {
            if (response['data'][0].Message != "Task Already Exist") {
              Model.TaskId = response['data'][0][''];
              Model.Status = 'Added to TS';
            } else {
              Model.Status = 'New';
            }
            this.saveRequirement(Model, 'REQUIREMENTS_UPDATE');

          } else {
          }
        });

        break;

      case 'Again Added to TS':

        Model.Status = 'Active';
        this.addRequirementsSubscription = this.timesheetService.add(null, Model, "TaskUpdateComplete", null).subscribe((response) => {
          if (response && response["successful"]) {
            Model.Status = 'Added to TS';
            this.saveRequirement(Model, 'REQUIREMENTS_UPDATE');

          } else {
          }
        });
        break;

      case 'Completed':

        Model.Status = 'Completed';
        this.addRequirementsSubscription = this.timesheetService.add(null, Model, "TaskUpdateComplete", null).subscribe((response) => {
          if (response && response["successful"]) {
            Model.Status = 'Completed';
            this.saveRequirement(Model, 'REQUIREMENTS_UPDATE');

          } else {
          }
        });
        break;



    }

  }

  showDetailModal(ReqDetail) {
    this.ReqDetail = ReqDetail;
    this.DetailModal.show();
  }

  showReqDescTable(model) {
    this.RequirementModel = model;
    this.onInitRequirementsDesc('ReqDescSelect', model.Id, model);
    this.DetailModal.show();
  }

  onInitRequirementsDesc(input, Id, model) {
    this.searchRequirementsSubscription = this.timesheetService.onInitRequirements(input, Id).subscribe((response) => {
      if (response && response["successful"]) {
        switch (input) {
          case 'ReqDescSelect':
            this.getReqDescTable = response["data"];
            this.getReqDescTable.forEach((element, Index) => {
              element['SrNo'] = Index + 1;
            });
            if (model && model !== '') {
              this.ToSkype = model.To ? model.To : this.ToSkype;
              var str = "R# : " + model.Id + "\nCreated On : " + "\nFrom : " + model.From + "\nTo : " + model.To + "\nPriority : " + model.Priority + "\nType : " + model.Type + "\nEffort : " + model.Effort +
                "\nProject Name : " + model.ProjectName + "\nPhase Name : " + model.PhaseName + "\nRequirement Name : " + model.RequirementName +
                "\nRequirement Detail : ";
              this.getReqDescTable.forEach(element => {
                str = str + "\n" + element['SrNo'] + " " + element['RequirementDesc'];
              });
              this.copyText(str);
            }
            break;
        }
      }
    });
  }

  //select  checkbox
  onChange($event: Event, model) {
    this.CheckBox = $event;
    this.RequirementID = this.RequirementModel.Id;
    this.RequirementModel.Id = model.Id;
    if (this.CheckBox['checked']) {
      this.RequirementModel.From = this.loggedInuserDetails.EmployeeName;
      this.saveRequirement(this.RequirementModel, 'ReqDescUpdateCheck');
    }
    else {
      this.saveRequirement(this.RequirementModel, 'ReqDescUpdateUnCheck');
    }
  }

  addRequiDetail() {
    this.addReqDetail = true;
    this.RequirementModel.RequirementDetail = null;
  }

  hideRequiDetailrow() {
    this.addReqDetail = false;
  }

  saveRequirementDetail(input) {
    this.saveRequirement(this.RequirementModel, input);
    this.addReqDetail = false;
  }

  deleteRequirementDetail(model, input) {
    this.RequirementID = this.RequirementModel.Id;
    this.RequirementModel.Id = model.Id;
    this.saveRequirement(this.RequirementModel, input);
  }

  closeDetailModal() {
    this.DetailModal.hide();
    this.addReqDetail = false;
    this.getReqDescTable = null;
  }

  //**********Close Modal for popup**********//
  closePopUpModal() {
    this.PopUpModal.hide();
    this.AddForm.resetForm();
  }

  deletePopup = (id, input) => {
    this.modalConfirmDelete.show();
    if (input == "REQUIREMENTS_DELETE") {
      this.RequirementModel.Id = id;
    }
  }

  getReqData(model) {
    this.onInitRequirementsDesc('ReqDescSelect', model.Id, model);
  }

  copyText(val: string) {
    if (val) {
      let selBox = document.createElement('textarea');
      selBox.style.position = 'fixed';
      selBox.style.left = '0';
      selBox.style.top = '0';
      selBox.style.opacity = '0';
      selBox.value = val;
      document.body.appendChild(selBox);
      selBox.focus();
      selBox.select();
      document.execCommand('copy');
      document.body.removeChild(selBox);
    }
  }
  // filtering

  showAll() {
    this.getRequirements = this.getRequirementsFixed;
    this.FilteredTypes = new Set(this.getRequirements.map((e) => { return e.Type }));
    this.FilteredProjects = new Set(this.getRequirements.map((e) => { return e.ProjectName }));
    this.FilteredPhases = new Set(this.getRequirements.map((e) => { return e.PhaseName }));
    this.FilteredFrom = new Set(this.getRequirements.map((e) => { return e.From }));
    this.FilteredTo = new Set(this.getRequirements.map((e) => { return e.To }));
    this.FilteredPriority = new Set(this.getRequirements.map((e) => { return e.Priority }));
    this.FilteredStatus = new Set(this.getRequirements.map((e) => { return e.Status }));
  }

  filterType2(data) {
    this.getRequirements = this.getRequirements.filter(x => x['Type'] === data);
    this.FilteredTypes = new Set(this.getRequirements.map((e) => { return e.Type }));
    this.FilteredProjects = new Set(this.getRequirements.map((e) => { return e.ProjectName }));
    this.FilteredPhases = new Set(this.getRequirements.map((e) => { return e.PhaseName }));
    this.FilteredFrom = new Set(this.getRequirements.map((e) => { return e.From }));
    this.FilteredTo = new Set(this.getRequirements.map((e) => { return e.To }));
    this.FilteredPriority = new Set(this.getRequirements.map((e) => { return e.Priority }));
    this.FilteredStatus = new Set(this.getRequirements.map((e) => { return e.Status }));
  }

  filterProject2(data) {
    this.getRequirements = this.getRequirements.filter(x => x['ProjectName'] === data);
    this.FilteredTypes = new Set(this.getRequirements.map((e) => { return e.Type }));
    this.FilteredProjects = new Set(this.getRequirements.map((e) => { return e.ProjectName }));
    this.FilteredPhases = new Set(this.getRequirements.map((e) => { return e.PhaseName }));
    this.FilteredFrom = new Set(this.getRequirements.map((e) => { return e.From }));
    this.FilteredTo = new Set(this.getRequirements.map((e) => { return e.To }));
    this.FilteredPriority = new Set(this.getRequirements.map((e) => { return e.Priority }));
    this.FilteredStatus = new Set(this.getRequirements.map((e) => { return e.Status }));
  }


  filterPhase2(data) {
    this.getRequirements = this.getRequirements.filter(x => x['PhaseName'] === data);
    this.FilteredTypes = new Set(this.getRequirements.map((e) => { return e.Type }));
    this.FilteredProjects = new Set(this.getRequirements.map((e) => { return e.ProjectName }));
    this.FilteredPhases = new Set(this.getRequirements.map((e) => { return e.PhaseName }));
    this.FilteredFrom = new Set(this.getRequirements.map((e) => { return e.From }));
    this.FilteredTo = new Set(this.getRequirements.map((e) => { return e.To }));
    this.FilteredPriority = new Set(this.getRequirements.map((e) => { return e.Priority }));
    this.FilteredStatus = new Set(this.getRequirements.map((e) => { return e.Status }));
  }

  filterFrom2(data) {
    this.getRequirements = this.getRequirements.filter(x => x['From'] === data);
    this.FilteredTypes = new Set(this.getRequirements.map((e) => { return e.Type }));
    this.FilteredProjects = new Set(this.getRequirements.map((e) => { return e.ProjectName }));
    this.FilteredPhases = new Set(this.getRequirements.map((e) => { return e.PhaseName }));
    this.FilteredFrom = new Set(this.getRequirements.map((e) => { return e.From }));
    this.FilteredTo = new Set(this.getRequirements.map((e) => { return e.To }));
    this.FilteredPriority = new Set(this.getRequirements.map((e) => { return e.Priority }));
    this.FilteredStatus = new Set(this.getRequirements.map((e) => { return e.Status }));
  }

  filterTo2(data) {
    this.getRequirements = this.getRequirements.filter(x => x['To'] === data);
    this.FilteredTypes = new Set(this.getRequirements.map((e) => { return e.Type }));
    this.FilteredProjects = new Set(this.getRequirements.map((e) => { return e.ProjectName }));
    this.FilteredPhases = new Set(this.getRequirements.map((e) => { return e.PhaseName }));
    this.FilteredFrom = new Set(this.getRequirements.map((e) => { return e.From }));
    this.FilteredTo = new Set(this.getRequirements.map((e) => { return e.To }));
    this.FilteredPriority = new Set(this.getRequirements.map((e) => { return e.Priority }));
    this.FilteredStatus = new Set(this.getRequirements.map((e) => { return e.Status }));
  }

  filterPriority2(data) {
    this.getRequirements = this.getRequirements.filter(x => x['Priority'] === data);
    this.FilteredTypes = new Set(this.getRequirements.map((e) => { return e.Type }));
    this.FilteredProjects = new Set(this.getRequirements.map((e) => { return e.ProjectName }));
    this.FilteredPhases = new Set(this.getRequirements.map((e) => { return e.PhaseName }));
    this.FilteredFrom = new Set(this.getRequirements.map((e) => { return e.From }));
    this.FilteredTo = new Set(this.getRequirements.map((e) => { return e.To }));
    this.FilteredPriority = new Set(this.getRequirements.map((e) => { return e.Priority }));
    this.FilteredStatus = new Set(this.getRequirements.map((e) => { return e.Status }));
  }

  filterStatus2(data) {
    this.getRequirements = this.getRequirements.filter(x => x['Status'] === data);
    this.FilteredTypes = new Set(this.getRequirements.map((e) => { return e.Type }));
    this.FilteredProjects = new Set(this.getRequirements.map((e) => { return e.ProjectName }));
    this.FilteredPhases = new Set(this.getRequirements.map((e) => { return e.PhaseName }));
    this.FilteredFrom = new Set(this.getRequirements.map((e) => { return e.From }));
    this.FilteredTo = new Set(this.getRequirements.map((e) => { return e.To }));
    this.FilteredPriority = new Set(this.getRequirements.map((e) => { return e.Priority }));
    this.FilteredStatus = new Set(this.getRequirements.map((e) => { return e.Status }));
  }

  // end filtering

  sendSkype(SkypeId){
    var str = "GO to Skype";
    var result = str.link('skype:'+SkypeId+'?chat');
    document.getElementById("demo").innerHTML = result;
  }

  // ******* Successfully and unsuccessfully message hide click on screen ********//
  @HostListener('document:click', ['$event'])
  documentClick(event: MouseEvent) {
    this.successmsg = '';
    this.unsuccessmsg = '';
  }
  //*****unique id corresponding to the item*****//
  trackByFn(item) {
    return item.id;
  }

  ngOnDestroy() {
    this.searchRequirementsSubscription ? this.searchRequirementsSubscription.unsubscribe() : null;
    this.addRequirementsSubscription ? this.addRequirementsSubscription.unsubscribe() : null;
  }
}
