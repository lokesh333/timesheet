import { Component, OnInit, HostListener } from '@angular/core';
import { IPaySlip } from 'src/app/Layout/Models/timesheet.model';
import { Subscription } from 'rxjs';
import { TimesheetService } from 'src/app/Layout/Services/timesheet.service';
import { DatePipe } from '@angular/common';
import { Moment } from 'moment';
import { MatDatepicker } from '@angular/material';
import { FileAttachment } from '../../SharedComponents/file-upload/file-attachment.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PayslipPdfService } from 'src/app/Layout/Services/payslip-pdf.service';

@Component({
  selector: 'app-pay-slip',
  templateUrl: './pay-slip.component.html',
  styleUrls: ['./pay-slip.component.scss']
})
export class PaySlipComponent implements OnInit {
  public PaySlipModel: IPaySlip = <IPaySlip>{};
  public PaySlipSelectSubscription: Subscription;
  public PaySlipInsertSubscription: Subscription;
  public successmsg: string;
  public getEmployeePaySlip: any;
  public EditAll: boolean = false;
  public choseMonthN: Date;
  public choseMonth: string;
  public uploadExcelFile: FileAttachment[] = [];
  public getEmployeePublishedPaySlip: any;
  public loggedInuserDetails: any;
  public getOneEmpPublishedPaySlip: any;

  constructor(
    private timesheetService: TimesheetService,
    private _snackBar: MatSnackBar,
    private payslipPdfService: PayslipPdfService,
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.loggedInuserDetails = JSON.parse(localStorage.getItem('currentUser'));
    this.loggedInuserDetails = this.loggedInuserDetails[0];
    if (this.loggedInuserDetails.UserType == 'Employee') {
      this.onInitOneEmpPublishedPaySlips("SelectOneEmpPublishedPaySlips", this.loggedInuserDetails.EmployeeId);
    }
  }

  selectionChange(a) {
    switch (a.selectedStep.label) {
      case "Details": this.choseMonth ? this.onInitPaySlip('SelectPaySlip', this.choseMonth + '-02') : null; break;
      case "Published": this.choseMonth ? this.onInitPublishedPaySlips('SelectPublishedPaySlips', this.choseMonth + '-02') : null; break;
    }
  }

  //  start details
  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    datepicker.close();
    this.choseMonthN = new Date(this.datePipe.transform(normalizedMonth.toString(), "yyyy-MM-dd"));
    this.choseMonth = this.datePipe.transform(normalizedMonth.toString(), "yyyy-MM");
    this.onInitPaySlip('SelectPaySlip', this.choseMonth + '-02');
    this.onInitPublishedPaySlips('SelectPublishedPaySlips', this.choseMonth + '-02')
  }



  //*****edit payslip details for update*****//
  editPayslip = (p) => {
    let index = this.getEmployeePaySlip.findIndex(x => x['EmployeeId'] === p['EmployeeId']);
    (index !== -1) ? this.getEmployeePaySlip[index]['editField'] = true : null;
    (index !== -1) ? this.getEmployeePaySlip[index]['edited'] = true : null;
    this.getEmployeePaySlip.forEach((element, Ind) => {
      if (Ind != index)
        element['edited'] = false;
    });
  }

  onInitPaySlip(input, Month) {
    this.PaySlipSelectSubscription = this.timesheetService.onInitPaySlip(input, Month, null).subscribe((response) => {
      if (response && response["successful"]) {
        switch (input) {
          case 'SelectPaySlip':
            this.getEmployeePaySlip = response['data'][0];
            this.getEmployeePaySlip.forEach((element, Ind) => {
              element['edited'] = true;
            });
            break;
        }
      }
    });
  }

  savePaySlip(Model, Input) {
    if (Input != 'PublishPaySlip' && Input != 'UnPublishPaySlip') {
      Model.Id ? Input = 'UpdatePaySlip' : Input = 'InsertPaySlip';
    }

    this.PaySlipModel.Month = new Date(this.choseMonth + '-02');
    Model.Month = this.choseMonth + '-02';
    this.PaySlipInsertSubscription = this.timesheetService.savePaySlip(Model, Input).subscribe((response) => {
      if (response && response["successful"]) {
        switch (Input) {
          case 'InsertPaySlip':
            let index = this.getEmployeePaySlip.findIndex(x => x['Id'] === Model['Id']);
            (index !== -1) ? this.getEmployeePaySlip[index]['editField'] = false : null;
            this.onInitPaySlip('SelectPaySlip', this.choseMonth + '-02');
            break;
          case 'UpdatePaySlip':
            let index2 = this.getEmployeePaySlip.findIndex(x => x['Id'] === Model['Id']);
            (index2 !== -1) ? this.getEmployeePaySlip[index2]['editField'] = false : null;
            this.onInitPaySlip('SelectPaySlip', this.choseMonth + '-02');
            break;
          case 'PublishPaySlip':
            this.onInitPaySlip('SelectPaySlip', this.choseMonth + '-02');
            break;
          case 'UnPublishPaySlip':
            this.onInitPaySlip('SelectPaySlip', this.choseMonth + '-02');
            break;
        }
      }
    });
  }

  cancelEdit() {
    this.onInitPaySlip('SelectPaySlip', this.choseMonth + '-02');
  }

  publishAll() {
    this.getEmployeePaySlip.forEach((element, Ind) => {
      if (element['Status'] == 'New') {
        this.savePaySlip(element, 'PublishPaySlip')
      }
    });
    this.onInitPaySlip('SelectPaySlip', this.choseMonth + '-02');
  }

  unPublishAll() {
    this.getEmployeePaySlip.forEach((element, Ind) => {
      if (element['Status'] == 'Published') {
        this.savePaySlip(element, 'UnPublishPaySlip')
      }
    });
    this.onInitPaySlip('SelectPaySlip', this.choseMonth + '-02');
    this.onInitPublishedPaySlips('SelectPublishedPaySlips', this.choseMonth + '-02')
  }

  // end details
  // start published

  onInitPublishedPaySlips(input, Month) {
    this.PaySlipSelectSubscription = this.timesheetService.onInitPaySlip(input, Month, null).subscribe((response) => {
      if (response && response["successful"]) {
        switch (input) {
          case 'SelectPublishedPaySlips':
            this.getEmployeePublishedPaySlip = (response['data'] && response['data'][0]) ? response['data'][0] : [];
            break;
        }
      }
    });
  }

  // end published
  // start employee
  onInitOneEmpPublishedPaySlips(input, EmployeeId) {
    this.PaySlipSelectSubscription = this.timesheetService.onInitPaySlip(input, null, EmployeeId).subscribe((response) => {
      if (response && response["successful"]) {
        switch (input) {
          case 'SelectOneEmpPublishedPaySlips':
            this.getOneEmpPublishedPaySlip = (response['data'] && response['data'][0]) ? response['data'][0] : [];
            break;
        }
      }
    });
  }
  // end employee

  importPayslipDetails = () => {
    this.timesheetService.importPayslipDetails(this.uploadExcelFile, this.choseMonth + '-02').subscribe((response) => {
      let message = ((response && response !== '' && !response['exception'])) ? response.toString() : 'Could Not Import File.';
      this._snackBar.open(message, 'Close', { duration: 2000 });
      this.uploadExcelFile = [];
      this.onInitPaySlip('SelectPaySlip', this.choseMonth + '-02');
    });
  }

  genratePdf(Model, Input) {
    let Data = [];
    Data['PayslipReference'] = '#' + Model.EmpId + (Model.MonthNum < 10 ? '0' + Model.MonthNum : Model.MonthNum) + Model.Year;
    Data['Month'] = Model.MonthName;
    Data['Year'] = Model.Year;
    Data['EmpId'] = Model.EmpId;
    Data['EmployeeName'] = Model.EmployeeName;
    Data['Designation'] = Model.Designation;
    Data['Department'] = Model.Department;
    Data['GrossSalary'] = Model.Gross;
    Data['Allowance'] = Model.Allowance;
    Data['Deduction'] = Model.Deduction;
    Data['LossOfPay'] = Model.LossOfPay;
    Data['NetSalary'] = Model.Net;
    Data['EmployeeId'] = Model.EmployeeId;
    this.payslipPdfService.CreatePdf(Data).then((response) => {
      if (response) {
        this.timesheetService.sevice(Model.Month, Model.EmployeeId, response, this.loggedInuserDetails.EmployeeId, Input).subscribe(resp => {
          if (resp && resp["successful"]) {
            this._snackBar.open('Mail Sent', 'Close', { duration: 1300 });
          }
        });
      }

    });
  }


  // ******* Successfully and unsuccessfully message hide click on screen ********//
  @HostListener('document:click', ['$event'])
  documentClick(event: MouseEvent) {
    this.successmsg = '';
    this.successmsg = '';
  }
  //*****unique id corresponding to the item*****//
  trackByFn(item) {
    return item.id;
  }

  ngOnDestroy() {
    this.PaySlipSelectSubscription ? this.PaySlipSelectSubscription.unsubscribe() : null;
    this.PaySlipInsertSubscription ? this.PaySlipInsertSubscription.unsubscribe() : null;
  }

}
