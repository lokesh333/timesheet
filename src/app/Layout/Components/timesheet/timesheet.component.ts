import { Component, OnInit, OnDestroy, ViewChild, HostListener } from '@angular/core';
import { TimesheetService } from 'src/app/Layout/Services/timesheet.service';
import { IAddModel, ILogin } from 'src/app/Layout/Models/timesheet.model';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { ModalDirective } from 'angular-bootstrap-md';
import { Router } from '@angular/router';
import { RequiremntsComponent } from './requiremnts/requiremnts.component';
import { Moment } from 'moment';
import { MatDatepicker } from '@angular/material';
import { SharedService } from '../../Services/shared.service';
@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent implements OnInit, OnDestroy {
  @ViewChild('Adding', { static: false }) public Adding: any;
  @ViewChild('AddForm', { static: false }) public AddForm: any;
  @ViewChild('DailyForm', { static: false }) public DailyForm: any;
  @ViewChild('ChangePwdForm', { static: false }) public ChangePwdForm: any;
  @ViewChild('PopUpModal', { static: false }) public PopUpModal: ModalDirective;
  @ViewChild('DailyPopUpModal', { static: false }) public DailyPopUpModal: ModalDirective;
  @ViewChild('PopupNotes', { static: false }) public PopupNotes: ModalDirective;
  @ViewChild('ChangePWDPopUpModal', { static: false }) public ChangePWDPopUpModal: ModalDirective;
  @ViewChild('modalConfirmDelete', { static: false }) public modalConfirmDelete: ModalDirective;
  @ViewChild('requiremnts', { static: false }) requiremnts: RequiremntsComponent;
  public LoginModel: ILogin = <ILogin>{};
  public loggedInuserDetails: any = [];
  public searchTasksOnInitSubscription: Subscription;
  public getDailyTasks: string[] = []
  public getYestTasks: string[] = [];
  public getWeeklyTasks: string[] = [];
  public getMonthlyTasks: string[] = [];
  public getProjects: string[] = [];
  public getWeeklyTasksFixed: string[] = [];
  public getProjectFixed: string[] = [];
  public getPhaseFixed: string[] = [];
  public getProjectFixed2: string[] = [];
  public getPhaseFixed2: string[] = [];
  public getPhases2Fixed: string[] = [];
  public getProjectFixed3: string[] = [];
  public getPhaseFixed3: string[] = [];
  public getPhases: string[] = [];
  public getTasks: string[] = [];
  public PhaseList: string[] = [];
  public getTasks2Fixedhigh: string[] = [];
  public TaskList: string[] = [];
  public PhaseList3: string[] = [];
  public getEmployees: string[] = [];
  public getAllUsers: string[] = [];
  public getRangeTasks: string[] = [];
  public getRangeTasksFixed: string[] = [];
  public getTasks2Fixed: string[] = [];
  public unsuccessmsg: string = "";
  public successmsg: string = "";
  public yest: boolean = false;
  public choseDateN: Date;
  public choseDateN_1: Date;
  public choseDate: string;
  public chosenDate: Date;
  public choseDateD: string;
  public chosenDateD: Date;
  public chosenDate_1: Date;
  public chosenDate_2: Date;
  public chosenDate_3: Date;
  public chosenDate_4: Date;
  public chosenDate_5: Date;
  public chosenDate_6: Date;
  public chosenDate_7: Date;
  public choseMonthN: Date;
  public yesterdayN: Date;
  public today: string;
  public yesterday: string;
  public UserType: string;
  public today_s: number;
  public t_1_s: number;
  public t_2_s: number;
  public t_3_s: number;
  public t_4_s: number;
  public t_5_s: number;
  public t_6_s: number;
  public tot: number[] = [];
  public Total_Hours_s: number;
  public EmployeeId: number;
  public FilterStatus: Set<any>;
  public FilterProjects: Set<any>;
  public FilterPhases: Set<any>;
  public FilterTasks: Set<any>;
  public FilterProjects2: Set<any>;
  public FilterPhases2: Set<any>;
  public FilterTasks2: Set<any>;
  public FilterProjects3: Set<any>;
  public FilterPhases3: Set<any>;
  public FilterTasks3: Set<any>;
  public FilterProjects4: Set<any>;
  public addRow: boolean = false;
  public next: boolean = false;
  public addModel: IAddModel = <IAddModel>{};
  public MonthDates: string[] = [];
  public MonthDates_h: number[] = [];
  public MonthlyTotalHoursSum: number;
  public Description: string = "";
  public RangeEmployees: string = "";
  public choseMonth: string;
  public RefreshMonth: string;
  public m1: boolean;
  public m2: boolean;
  public m3: boolean;
  public searchOnInitSubscription: Subscription;
  public addSubscription: Subscription;
  public getProjects2: string[] = [];
  public getPhases2: string[] = [];
  public getTasks2: string[] = [];
  public getresult: boolean = false;
  public addPopup: boolean = false;
  public type: string = "";
  public Title: string = "";
  public dateRange: any;
  public RangeEmployeeID: number;
  public FromDate: any;
  public ToDate: any;
  public SumTotalHours: number;
  public DailySumHours: number;
  public m: number;
  public n: number;
  public o: number;
  public flag: boolean = false;
  public reqc: number[] = [];
  public dateRangeFlag: boolean;
  public Prof: boolean = false;
  public AllPending: number;
  public SumAvgHrs: number;
  public SumTotalDays: number;
  GetDailyNotes: any;

  constructor(
    private timesheetService: TimesheetService,
    public router: Router,
    private sharedService: SharedService,
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.loggedInuserDetails = JSON.parse(localStorage.getItem('currentUser'));
    this.loggedInuserDetails = this.loggedInuserDetails[0];
    this.UserType = this.loggedInuserDetails.UserType;
    this.EmployeeId = this.loggedInuserDetails.EmployeeId;
    this.searchTasksOnInit('AllEmployees');
    this.searchTasksOnInit('AllUsers');
    this.today = this.datePipe.transform(new Date().toLocaleDateString(), "yyyy-MM-dd");
    this.choseDateN = new Date();
    this.yesterdayN = new Date();
    this.yesterdayN = new Date(this.choseDateN.getTime() - 3 * 24 * 60 * 60 * 1000);
    this.yesterday = this.datePipe.transform(this.yesterdayN.toLocaleDateString(), "yyyy-MM-dd");
    this.choseDateN_1 = new Date();
    this.choseDate = this.datePipe.transform(new Date().toLocaleDateString(), "yyyy-MM-dd");
    this.choseDateD = this.datePipe.transform(new Date().toLocaleDateString(), "yyyy-MM-dd");
    this.searchTasksOnInit('ProjectSelectActive');
    this.searchTasksOnInit('PhaseSelectActive');
    this.searchTasksOnInit('TaskSelectActive');
    this.chosenDate = new Date();
    this.chosenDateD = new Date();
    this.chosenDate = new Date(this.chosenDate.getTime() - 6 * 24 * 60 * 60 * 1000);
    this.chooseWeeklyDate(this.chosenDate);
    this.addModel.ProjectId = 0;
    this.searchMonthlyHoursOnInit('MonthlyView', this.today, null);
    this.searchOnInit("ProjectSelect");
    this.searchOnInit("PhaseSelect");
    this.searchOnInit("TaskSelect");
    this.RefreshMonth = this.datePipe.transform(new Date().toLocaleDateString(), "yyyy-MM");
    this.addModel.FromDate = new Date('2019-12-01');
    this.addModel.ToDate = new Date();
    this.searchTasksDateRange(this.addModel.FromDate, this.addModel.ToDate);
    this.addModel.RangeEmployeeID = null;
    this.searchTasksOnInit('DailyTaskSelect');
    this.searchWeekTasksOnInit('WeeklyView', this.datePipe.transform(this.chosenDate.toLocaleDateString(), "yyyy-MM-dd"), -333);
    this.sharedService.PendingNotListner.subscribe((item) => {
      if (item !== '') {
        this.AllPending = item;
      }
    });
  }

  tab(id) {
    switch (id.tab.textLabel) {
      case 'COMPANY':
        this.searchOnInit("TaskSelect");
        break;
      case 'DAILY TS':
        this.searchTasksOnInit('ProjectSelectActive');
        this.searchTasksOnInit('PhaseSelectActive');
        this.searchTasksOnInit('TaskSelectActive');
        break;
      case 'MY WORK':
        this.searchTasksDateRange(this.addModel.FromDate, this.addModel.ToDate);
        this.requirementCount();
        break;
      case 'MONTHLY VIEW':

        break;
      case 'WEEK VIEW':
        this.searchWeekTasksOnInit('WeeklyView', this.datePipe.transform(this.chosenDate.toLocaleDateString(), "yyyy-MM-dd"), -333);
        break;
      case 'REQUIREMENTS':
        this.requiremnts.callRequirementMethods();
        break;
    }

  }

  searchTasksOnInit = (input: string) => {
    if (this.loggedInuserDetails.UserType == 'Employee') {
      this.EmployeeId = this.loggedInuserDetails.EmployeeId;
    }
    this.searchTasksOnInitSubscription = this.timesheetService.searchTasksOnInit(this.EmployeeId, input, this.choseDateD).subscribe((response) => {
      if (response["successful"] && response["data"]) {

        switch (input) {
          case 'DailyTaskSelect':
            this.getDailyTasks = response["data"];
            this.DailySumHours = response["data"].reduce((a, b) => +a + +b["Hours"], 0);

            break;
          case 'YestTaskSelect':
            this.getYestTasks = response["data"];
            if (response["data"].length > 0) {
              this.yest = true;
            }

            this.getYestTasks.forEach(element => {
              element['PhaseList'] = this.getPhases.filter(x => x['ProjectId'] === element['ProjectId'])
              element['DailyTaskNotes'] = null;
              element['Hours'] = null;
              element['TaskList2'] = this.getTasks.filter(x => x['PhaseId'] === element['PhaseId'])
            });

            break;
          case 'ProjectSelectActive':
            this.getProjects = response["data"];

            break;
          case 'PhaseSelectActive':
            this.getPhases = response["data"];

            break;
          case 'TaskSelectActive':
            this.getTasks = response["data"];

            break;
          case 'AllEmployees':
            this.getEmployees = response["data"];

            break;
          case 'AllUsers':
            this.getAllUsers = response["data"];

            break;
        }
      } else {

      }
    });
  }

  profile() {
    this.Prof ? this.Prof = false : this.Prof = true;
  }

  onChangeProject = (ProjectId: number) => {
    ProjectId ? this.PhaseList = this.getPhases.filter(x => x['ProjectId'] === ProjectId) : this.PhaseList = [];
  }

  onChangePhase = (PhaseId: number) => {
    PhaseId ? this.TaskList = this.getTasks.filter(x => x['PhaseId'] === PhaseId) : this.TaskList = [];
  }

  onChangeProject2 = (ProjectId: number, model) => {
    ProjectId ? model.PhaseList = this.getPhases.filter(x => x['ProjectId'] === ProjectId) : model.PhaseList = [];
  }

  onChangePhase2 = (PhaseId: number, model) => {
    PhaseId ? model.TaskList2 = this.getTasks.filter(x => x['PhaseId'] === PhaseId) : model.TaskList2 = [];
  }

  onChangeProject3 = (ProjectId: number) => {
    ProjectId ? this.PhaseList3 = this.getPhases2.filter(x => x['ProjectId'] === ProjectId) : this.PhaseList3 = [];
  }

  showYest() {

    this.searchTasksOnInit('YestTaskSelect');
  }
  removeYest(DailyTaskId) {

    let index = this.getYestTasks.findIndex(x => x['DailyTaskId'] === DailyTaskId);
    if (index > -1) {
      this.getYestTasks.splice(index, 1);
    }
    if (this.getYestTasks.length === 0)
      this.yest = false;
  }
  saveYest(models, input) {
    this.yest = false;
    for (let i = 0; i < models.length; i++) {
      this.saveRow(models[i]);
    }
  }


  showRow() {
    this.addRow = true;
  }

  saveRow = (Model) => {
    this.addRow = false;
    if (this.loggedInuserDetails.UserType == 'Employee') {
      this.EmployeeId = this.loggedInuserDetails.EmployeeId;
    }
    this.searchTasksOnInitSubscription = this.timesheetService.add(this.EmployeeId, Model, "DailyTaskInsert", this.datePipe.transform(this.chosenDateD.toLocaleDateString(), "yyyy-MM-dd")).subscribe((response) => {
      if (response && response["successful"]) {
        this.addModel.ProjectId = null;
        this.addModel.PhaseId = null;
        this.addModel.TaskId = null;
        this.addModel.DailyTaskNotes = null;
        this.addModel.Hours = null;
        this.searchTasksOnInit("DailyTaskSelect");
        this.searchWeekTasksOnInit('WeeklyView', this.datePipe.transform(this.chosenDate.toLocaleDateString(), "yyyy-MM-dd"), this.EmployeeId);
        this.searchMonthlyHoursOnInit('MonthlyView', this.choseMonth + '-01', null);
        this.searchTasksDateRange(this.FromDate, this.ToDate);
      } else {
      }
    });
  }

  deleteRow = (Model, input) => {
    this.searchTasksOnInitSubscription = this.timesheetService.add(this.EmployeeId, Model, input, this.datePipe.transform(this.chosenDate.toLocaleDateString(), "yyyy-MM-dd")).subscribe((response) => {
      if (response && response["successful"]) {
        this.searchTasksOnInit("DailyTaskSelect");
        this.searchWeekTasksOnInit('WeeklyView', this.datePipe.transform(this.chosenDate.toLocaleDateString(), "yyyy-MM-dd"), this.EmployeeId);
        this.searchMonthlyHoursOnInit('MonthlyView', this.choseMonth + '-01', null);
        this.searchTasksDateRange(this.FromDate, this.ToDate);
      } else {
      }
    });
  }

  previousDay1 = () => {
    this.choseDateN = new Date(this.choseDateN.getTime() - 1 * 24 * 60 * 60 * 1000);
    this.chooseDate(this.choseDateN);
  }

  nextDay1 = () => {
    this.choseDateN = new Date(this.choseDateN.getTime() + 1 * 24 * 60 * 60 * 1000);
    this.chooseDate(this.choseDateN);
  }

  chooseDate = (date) => {
    this.addModel['startdate'] = new Date(date);
    this.choseDateN = date;
    this.choseDateN_1 = new Date(this.choseDateN);
    this.choseDateD = this.datePipe.transform(date.toLocaleDateString(), "yyyy-MM-dd");
    this.chosenDateD = date;
    this.searchTasksOnInit('DailyTaskSelect');
  }

  chooseWeeklyDate = (date) => {
    this.addModel.Date = new Date(date);
    this.chosenDate = date;
    this.chosenDate_1 = new Date(this.chosenDate);
    this.chosenDate_2 = new Date(this.chosenDate.getTime() + 1 * 24 * 60 * 60 * 1000);
    this.chosenDate_3 = new Date(this.chosenDate.getTime() + 2 * 24 * 60 * 60 * 1000);
    this.chosenDate_4 = new Date(this.chosenDate.getTime() + 3 * 24 * 60 * 60 * 1000);
    this.chosenDate_5 = new Date(this.chosenDate.getTime() + 4 * 24 * 60 * 60 * 1000);
    this.chosenDate_6 = new Date(this.chosenDate.getTime() + 5 * 24 * 60 * 60 * 1000);
    this.chosenDate_7 = new Date(this.chosenDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    this.searchWeekTasksOnInit('WeeklyView', this.datePipe.transform(this.chosenDate.toLocaleDateString(), "yyyy-MM-dd"), (this.getEmployees.filter(x => x['EmployeeId'] === this.EmployeeId).length == 0) ? -333 : this.EmployeeId);
  }

  selectEmployee = (id) => {
    this.EmployeeId = id;
    this.searchWeekTasksOnInit('WeeklyView', this.datePipe.transform(this.chosenDate.toLocaleDateString(), "yyyy-MM-dd"), this.EmployeeId);
  }

  refreshWeekView() {
    this.searchWeekTasksOnInit('WeeklyView', this.datePipe.transform(this.chosenDate.toLocaleDateString(), "yyyy-MM-dd"), (this.getEmployees.filter(x => x['EmployeeId'] === this.EmployeeId).length === 0) ? -333 : this.EmployeeId);
  }

  refreshMonthView() {
    this.searchMonthlyHoursOnInit('MonthlyView', this.RefreshMonth + '-01', null);
  }

  previousEmployee() {
    if (this.next) {
      let index = this.getEmployees.findIndex(x => x['EmployeeId'] === this.EmployeeId)
      if (index != 0) {
        let EmpId = this.getEmployees[index - 1]['EmployeeId'];
        if (EmpId > 2) {
          this.searchOtherTasks('DailyTaskSelect', EmpId);
        }
      }
    }
  }

  nextEmployee() {
    let index = this.getEmployees.findIndex(x => x['EmployeeId'] === this.EmployeeId)
    if (index != this.getEmployees.length - 1) {
      this.next = true;
      let EmpId = this.getEmployees[index + 1]['EmployeeId'];
      this.searchOtherTasks('DailyTaskSelect', EmpId);
    }
  }

  searchOtherTasks = (input: string, EmployeeId2) => {
    this.searchTasksOnInitSubscription = this.timesheetService.searchTasksOnInit(EmployeeId2, input, this.choseDateD).subscribe((response) => {
      if (response["successful"] && response["data"]) {
        switch (input) {
          case 'DailyTaskSelect':
            this.getDailyTasks = response["data"];
            this.EmployeeId = EmployeeId2;
            this.addModel.EmployeeId = EmployeeId2;
            break;
        }
      } else {
      }
    });
  }

  previousWeek = () => {
    this.chosenDate = new Date(this.chosenDate.getTime() - 7 * 24 * 60 * 60 * 1000);
    this.chooseWeeklyDate(this.chosenDate);
  }

  previousDay = () => {
    this.chosenDate = new Date(this.chosenDate.getTime() - 1 * 24 * 60 * 60 * 1000);
    this.chooseWeeklyDate(this.chosenDate);
  }

  nextDay = () => {
    this.chosenDate = new Date(this.chosenDate.getTime() + 1 * 24 * 60 * 60 * 1000);
    this.chooseWeeklyDate(this.chosenDate);
  }

  nextWeek = () => {
    this.chosenDate = new Date(this.chosenDate.getTime() + 7 * 24 * 60 * 60 * 1000);
    this.chooseWeeklyDate(this.chosenDate);
  }

  searchWeekTasksOnInit = (input, date, EmployeeId) => {
    if (this.loggedInuserDetails.UserType == 'Employee') {
      EmployeeId = this.loggedInuserDetails.EmployeeId;
    }
    this.searchTasksOnInitSubscription = this.timesheetService.searchTasksOnInit(EmployeeId, input, date).subscribe((response) => {
      if (response["successful"] && response["data"]) {
        response["data"] = response["data"].filter(x => x['Total_Hours'] != null);
        switch (input) {
          case 'WeeklyView':
            this.getWeeklyTasks = response["data"];
            this.getWeeklyTasksFixed = response["data"];
            this.today_s = response["data"].reduce((a, b) => +a + +b["today"], 0);
            this.t_1_s = response["data"].reduce((a, b) => +a + +b["t_1"], 0);
            this.t_2_s = response["data"].reduce((a, b) => +a + +b["t_2"], 0);
            this.t_3_s = response["data"].reduce((a, b) => +a + +b["t_3"], 0);
            this.t_4_s = response["data"].reduce((a, b) => +a + +b["t_4"], 0);
            this.t_5_s = response["data"].reduce((a, b) => +a + +b["t_5"], 0);
            this.t_6_s = response["data"].reduce((a, b) => +a + +b["t_6"], 0);
            this.Total_Hours_s = response["data"].reduce((a, b) => +a + +b["Total_Hours"], 0).toFixed(1);
            this.FilterProjects = new Set(response["data"].map((e) => { return e.ProjectName }));
            break;
        }
      } else {
      }
    });
  }

  filterProject = (name) => {
    if (name == "All Projects")
      this.getWeeklyTasks = this.getWeeklyTasksFixed;
    else
      this.getWeeklyTasks = this.getWeeklyTasksFixed.filter(x => x['ProjectName'] === name);
    this.getProjectFixed = this.getWeeklyTasksFixed.filter(x => x['ProjectName'] === name);
    this.FilterPhases = new Set(this.getProjectFixed.map((e) => { return e['PhaseName'] }));
    this.updateSum();
  }

  filterPhase = (name) => {
    if (name == "All Phases") {
      if (this.getProjectFixed.length > 0)
        this.getWeeklyTasks = this.getProjectFixed;
    }
    else
      this.getWeeklyTasks = this.getProjectFixed.filter(x => x['PhaseName'] === name)
    this.getPhaseFixed = this.getProjectFixed.filter(x => x['PhaseName'] === name);
    this.FilterTasks = new Set(this.getPhaseFixed.map((e) => { return e['TaskName'] }));
    this.updateSum();
  }

  filterTask = (name) => {
    if (name == "All Tasks") {
      if (this.getPhaseFixed.length > 0)
        this.getWeeklyTasks = this.getPhaseFixed;
    }
    else
      this.getWeeklyTasks = this.getPhaseFixed.filter(x => x['TaskName'] === name)
    this.updateSum();
  }

  updateSum() {
    this.today_s = this.getWeeklyTasks.reduce((a, b) => +a + +b["today"], 0);
    this.t_1_s = this.getWeeklyTasks.reduce((a, b) => +a + +b["t_1"], 0);
    this.t_2_s = this.getWeeklyTasks.reduce((a, b) => +a + +b["t_2"], 0);
    this.t_3_s = this.getWeeklyTasks.reduce((a, b) => +a + +b["t_3"], 0);
    this.t_4_s = this.getWeeklyTasks.reduce((a, b) => +a + +b["t_4"], 0);
    this.t_5_s = this.getWeeklyTasks.reduce((a, b) => +a + +b["t_5"], 0);
    this.t_6_s = this.getWeeklyTasks.reduce((a, b) => +a + +b["t_6"], 0);
    this.Total_Hours_s = this.getWeeklyTasks.reduce((a, b) => +a + +b["Total_Hours"], 0);
  }

  weeklyEmployee = (id) => {
    this.EmployeeId = id;
    this.searchWeekTasksOnInit('WeeklyView', this.today, this.EmployeeId);
  }

  mouseEnter(p, d) {
    this.searchTasksOnInitSubscription = this.timesheetService.searchTasksOnInit(p.EmployeeId, "DailyTaskSelect", d).subscribe((response) => {
      if (response["successful"] && response["data"]) {
        if (response["data"].length > 0)
          this.Description = response['data'][0]["EmployeeName"];
        for (let i = 0; i < response["data"].length; i++) {
          this.Description = this.Description + "\n" + response['data'][i]["ProjectName"] + " - " + response['data'][i]["PhaseName"] + " - " + response['data'][i]["TaskName"] + " - " + response['data'][i]["Hours"]
        }
        if (response["data"].length === 0)
          this.Description = 'NA'
      }
    });
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    datepicker.close();
    this.choseMonthN = new Date(this.datePipe.transform(normalizedMonth.toString(), "yyyy-MM-dd"));
    this.choseMonth = this.datePipe.transform(normalizedMonth.toString(), "yyyy-MM");
    this.RefreshMonth = this.choseMonth;
    this.searchMonthlyHoursOnInit('MonthlyView', this.choseMonth + '-01', null);
  }

  searchMonthlyHoursOnInit = (input, date, EmployeeId) => {
    this.searchTasksOnInitSubscription = this.timesheetService.searchTasksOnInit(EmployeeId, input, date).subscribe((response) => {
      if (response["successful"] && response["data"]) {

        switch (input) {
          case 'MonthlyView':
            this.choseMonthN = date;
            this.choseMonth = this.datePipe.transform(this.choseMonthN, "yyyy-MM");
            var yy = this.datePipe.transform(date, "yyyy"), mm = this.datePipe.transform(date, "MM");
            var yy_num = +yy, mm_num = +mm;
            var lastDay = new Date(yy_num, mm_num, 0);
            if (lastDay.getDate() == 28) {
              this.m1 = false; this.m2 = false; this.m3 = false;
              this.MonthDates = Array(28).fill(3).map((x, i) => 'd' + (i + 1));
              this.MonthDates_h = Array(28).fill(3).map((x, i) => i + 1);
            }
            else
              if (lastDay.getDate() == 29) {
                this.m1 = true; this.m2 = false; this.m3 = false;
                this.MonthDates = Array(29).fill(3).map((x, i) => 'd' + (i + 1));
                this.MonthDates_h = Array(29).fill(3).map((x, i) => i + 1);
              } else
                if (lastDay.getDate() == 30) {
                  this.m1 = true; this.m2 = true; this.m3 = false;
                  this.MonthDates = Array(30).fill(3).map((x, i) => 'd' + (i + 1));
                  this.MonthDates_h = Array(30).fill(3).map((x, i) => i + 1);
                }
                else {
                  this.m1 = true; this.m2 = true; this.m3 = true;
                  this.MonthDates = Array(31).fill(3).map((x, i) => 'd' + (i + 1));
                  this.MonthDates_h = Array(31).fill(3).map((x, i) => i + 1);
                }
            this.getMonthlyTasks = response["data"];
            let getheader = [];
            this.getMonthlyTasks.forEach(element => {
              getheader = element ? Object.keys(element) : [];
              let count = 0;
              getheader.forEach(itm => {
                if (itm && itm !== 'EmployeeId' && itm !== 'EName' && itm !== 'TotalHoursSum' && element[itm] && Number(element[itm]) != 0) {
                  count++;
                }
              });
              element['TotalDays'] = count;
            });
            for (let i of this.MonthDates_h) {
              this.tot[i] = response["data"].reduce((a, b) => +a + +b[this.MonthDates[i - 1]], 0);
              this.tot[i] = this.tot[i] ? this.tot[i] : null;
            }
            this.MonthlyTotalHoursSum = response["data"].reduce((a, b) => +a + +b['TotalHoursSum'], 0);
            this.SumAvgHrs = response["data"].reduce((a, b) => +a + +b['TotalHoursSum'] / b['TotalDays'], 0);
            this.SumTotalDays = response["data"].reduce((a, b) => +a + +b['TotalDays'], 0);
            break;
        }
      }
    });
  }

  hideRow = () => {
    this.addRow = false;
  }

  // company

  searchOnInit = (input: string) => {
    this.searchOnInitSubscription = this.timesheetService.searchOnInit(input).subscribe((response) => {
      if (response["successful"] && response["data"]) {
        this.getresult = true;
        switch (input) {
          case 'ProjectSelect':
            this.getProjects2 = response["data"];
            this.getProjects2.forEach((element, Index) => {
              element['SrNo'] = Index + 1;
            });
            break;
          case 'PhaseSelect':
            this.getPhases2 = response["data"];
            this.getPhases2Fixed = response["data"];
            this.FilterProjects4 = new Set(response["data"].map((e) => { return e.ProjectName }));
            this.getPhases2.forEach((element, Index) => {
              element['SrNo'] = Index + 1;
            });
            break;
          case 'TaskSelect':
            this.getTasks2 = response["data"];
            this.getTasks2Fixedhigh = response['data'];
            this.getTasks2Fixed = response['data'];
            this.FilterProjects3 = new Set(this.getTasks2Fixed.map((e) => { return e['ProjectName'] }));
            this.FilterStatus = new Set(response["data"].map((e) => { return e.Status }));
            this.getTasks2.forEach((element, Index) => {
              element['SrNo'] = Index + 1;
            });
            break;
        }
      } else {
        this.getresult = false;
      }
    });
  }

  filterStatus = (name) => {
    if (name == "All Status") {
      this.getTasks2 = this.getTasks2Fixedhigh;
      this.getTasks2Fixed = this.getTasks2Fixedhigh;
    }
    else {
      this.getTasks2Fixed = this.getTasks2Fixedhigh.filter(x => x['Status'] === name);
      this.getTasks2 = this.getTasks2Fixed;
    }
    this.FilterProjects3 = new Set(this.getTasks2Fixed.map((e) => { return e['ProjectName'] }));
  }


  filterProject3 = (name) => {
    if (name == "All Projects")
      this.getTasks2 = this.getTasks2Fixed;
    else
      this.getTasks2 = this.getTasks2Fixed.filter(x => x['ProjectName'] === name);
    this.getProjectFixed3 = this.getTasks2Fixed.filter(x => x['ProjectName'] === name);
    this.FilterPhases3 = new Set(this.getProjectFixed3.map((e) => { return e['PhaseName'] }));

  }

  filterPhase3 = (name) => {
    if (name == "All Phases") {
      if (this.getProjectFixed3.length > 0)
        this.getTasks2 = this.getProjectFixed3;
    } else
      this.getTasks2 = this.getProjectFixed3.filter(x => x['PhaseName'] === name)
    this.getPhaseFixed3 = this.getProjectFixed3.filter(x => x['PhaseName'] === name);
    this.FilterTasks3 = new Set(this.getPhaseFixed3.map((e) => { return e['TaskName'] }));
  }

  filterTask3 = (name) => {
    if (name == "All Tasks") {
      if (this.getPhaseFixed3.length > 0)
        this.getTasks2 = this.getPhaseFixed3;
    } else
      this.getTasks2 = this.getPhaseFixed3.filter(x => x['TaskName'] === name)
    this.updateSumTotalHours();
  }


  filterProject4 = (name) => {
    if (name == "All Projects")
      this.getPhases2 = this.getPhases2Fixed;
    else
      this.getPhases2 = this.getPhases2Fixed.filter(x => x['ProjectName'] === name);
  }

  //**********This method is used to ADD  **********//
  add = (Model, input) => {
    this.addSubscription = this.timesheetService.add(null, Model, input + "Insert", this.datePipe.transform(this.chosenDate.toLocaleDateString(), "yyyy-MM-dd")).subscribe((response) => {
      if (response && response["successful"]) {
        if (input != 'Task') {
          this.AddForm.resetForm();
        }
        this.successmsg = response["data"][0]["Message"];
        this.searchOnInit(input + "Select");
        this.searchTasksOnInit(input + 'SelectActive');
        setTimeout(function () {
          this.successmsg = " ";
        }, 1000);
      } else {
        this.unsuccessmsg = "UnSuccessful";
        setTimeout(function () {
          this.unsuccessmsg = " ";
        }, 1000);
      }
    });

  }

  //**********This method is used to UPDATE  **********//
  update = (Model, input) => {
    this.addSubscription = this.timesheetService.add(null, Model, input + "Update", this.datePipe.transform(this.chosenDate.toLocaleDateString(), "yyyy-MM-dd")).subscribe((response) => {
      if (response && response["successful"]) {
        if (input == 'DailyTask')
          this.DailyForm.resetForm();
        else
          this.AddForm.resetForm();
        this.successmsg = response["data"][0]["Message"];
        this.searchOnInit(input + "Select");
        this.searchTasksOnInit(input + 'SelectActive');
        this.searchTasksOnInit(input + 'Select');
      } else {
        this.unsuccessmsg = "UnSuccessful";
      }
    });
  }

  //**********This method is used to DELETE  **********//
  delete = (Model, input) => {
    this.addSubscription = this.timesheetService.add(null, Model, input + "Delete", this.datePipe.transform(this.chosenDate.toLocaleDateString(), "yyyy-MM-dd")).subscribe((response) => {
      if (response && response["successful"]) {
        this.modalConfirmDelete.hide();
        this.searchOnInit(input + "Select");
        this.searchTasksOnInit(input + 'SelectActive');
      } else {
      }
    });
  }

  //**********show popup**********//
  createPopup = (model, input) => {
    if (input == "Update Project") {
      this.addModel.ProjectId = model.ProjectId;
      this.addModel.ProjectName = model.ProjectName;
      this.addModel.Status = model.Status;
    }
    if (input == "Update Phase") {
      this.addModel.PhaseId = model.PhaseId;
      this.addModel.ProjectId = model.ProjectId;
      this.addModel.ProjectName = model.ProjectName;
      this.addModel.PhaseName = model.PhaseName;
      this.addModel.Status = model.Status;
    }
    if (input == "Update Task") {
      this.addModel.PhaseId = model.PhaseId;
      this.addModel.ProjectId = model.ProjectId;
      this.addModel.ProjectName = model.ProjectName;
      this.addModel.PhaseName = model.PhaseName;
      this.addModel.TaskId = model.TaskId;
      this.addModel.TaskName = model.TaskName;
      this.addModel.Status = model.Status;
      this.onChangeProject3(model.ProjectId);
    }

    if (input == "DailyTaskUpdate") {
      this.addModel.PhaseId = model.PhaseId;
      this.addModel.ProjectId = model.ProjectId;
      this.addModel.ProjectName = model.ProjectName;
      this.addModel.PhaseName = model.PhaseName;
      this.addModel.TaskId = model.TaskId;
      this.addModel.TaskName = model.TaskName;
      this.addModel.DailyTaskNotes = model.DailyTaskNotes;
      this.addModel.DailyTaskId = model.DailyTaskId;
      this.addModel.Hours = model.Hours;
      this.onChangeProject(model.ProjectId);
      this.onChangePhase(model.PhaseId);
      this.DailyPopUpModal.show();
    }
    if (input != "DailyTaskUpdate") {
      this.PopUpModal.show();
    }
    this.addPopup = true;
    this.Title = input;
    this.type = input;
  }



  //**********Close Modal for popup**********//
  closePopUpModal() {
    this.addPopup = false;
    this.DailyForm.resetForm();
    this.addModel = <IAddModel>{};
    this.PopUpModal.hide();
    this.successmsg = "";
    this.unsuccessmsg = "";
  }

  closeDailyPopUpModal() {
    this.DailyForm.resetForm();
    this.addModel = <IAddModel>{};
    this.DailyPopUpModal.hide();
    this.successmsg = "";
    this.unsuccessmsg = "";
  }
  //**********Open Dialog for Delete**********//
  deletePopup = (id, input) => {
    this.modalConfirmDelete.show();
    if (input == "Delete Project") {
      this.addModel.ProjectId = id;
    }
    if (input == "Delete Phase") {
      this.addModel.PhaseId = id;
    }
    if (input == "Delete Task") {
      this.addModel.TaskId = id;
    }
    this.type = input;
  }

  // end company

  // login


  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  // end login

  // start range view


  dateRangeEvent = ($event) => {
    this.dateRange = $event;
    if (this.dateRange.startDate && this.dateRange.endDate) {
      this.FromDate = this.dateRange.startDate._d.toISOString();
      this.ToDate = this.dateRange.endDate._d.toISOString();
      this.searchTasksDateRange(this.FromDate, this.ToDate);
      this.dateRangeFlag = false;
    } else {

      this.dateRangeFlag = true;
    }
  }

  SelectRangeEmployee(id) {
    this.RangeEmployeeID = id;
    this.searchTasksDateRange(this.FromDate, this.ToDate);
    this.requirementCount();
  }

  searchTasksDateRange = (FromDate, ToDate) => {
    if (this.loggedInuserDetails.UserType == 'Employee') {
      this.RangeEmployeeID = this.loggedInuserDetails.EmployeeId;
    }
    this.FromDate = FromDate;
    this.ToDate = ToDate;
    this.addSubscription = this.timesheetService.searchTasksDateRange("RANGE VIEW", this.RangeEmployeeID, this.datePipe.transform(FromDate, "yyyy-MM-dd"), this.datePipe.transform(ToDate, "yyyy-MM-dd")).subscribe((response) => {
      if (response && response["successful"]) {
        this.getRangeTasksFixed = response['data'];
        this.getRangeTasks = response['data'];
        this.FilterProjects2 = new Set(response["data"].map((e) => { return e.ProjectName }));
        this.updateSumTotalHours();
      } else {
      }
    });
  }

  mouseOverRange(model) {
    this.addSubscription = this.timesheetService.mouseOverRange("MouseoverRANGEVIEW", model).subscribe((response) => {
      if (response["successful"] && response["data"]) {
        if (response["data"].length > 0)
          this.RangeEmployees = response['data'][0]["TaskName"];
        for (let i = 0; i < response["data"].length; i++) {
          this.RangeEmployees = this.RangeEmployees + "\n" + response['data'][i]["EmployeeName"] + " - " + response['data'][i]["Hours"]
        }
        if (response["data"].length === 0)
          this.RangeEmployees = 'NA'
      }
    });
  }

  showNotes(model) {
    this.PopupNotes.show();
    this.addSubscription = this.timesheetService.mouseOverRange("GETTASKNOTES", model).subscribe((response) => {
      if (response["successful"] && response["data"]) {
        this.GetDailyNotes = response['data'];
        this.Title = response['data'][0]['TaskName'];
      }
    });
  }

  closePopupNotes() {
    this.PopupNotes.hide();
  }

  updateSumTotalHours() {
    this.SumTotalHours = this.getRangeTasks.reduce((a, b) => +a + +b["TotalHours"], 0);
  }

  filterProject2 = (name) => {

    if (name == "All Projects")
      this.getRangeTasks = this.getRangeTasksFixed;

    else
      this.getRangeTasks = this.getRangeTasksFixed.filter(x => x['ProjectName'] === name);
    this.getProjectFixed2 = this.getRangeTasksFixed.filter(x => x['ProjectName'] === name);
    this.FilterPhases2 = new Set(this.getProjectFixed2.map((e) => { return e['PhaseName'] }));
    this.updateSumTotalHours();
  }

  filterPhase2 = (name) => {

    if (name == "All Phases") {
      if (this.getProjectFixed2.length > 0)
        this.getRangeTasks = this.getProjectFixed2;
    }
    else
      this.getRangeTasks = this.getProjectFixed2.filter(x => x['PhaseName'] === name)
    this.getPhaseFixed2 = this.getProjectFixed2.filter(x => x['PhaseName'] === name);
    this.FilterTasks2 = new Set(this.getPhaseFixed2.map((e) => { return e['TaskName'] }));
    this.updateSumTotalHours();
  }

  filterTask2 = (name) => {

    if (name == "All Tasks") {
      if (this.getPhaseFixed2.length > 0)
        this.getRangeTasks = this.getPhaseFixed2;
    }
    else
      this.getRangeTasks = this.getPhaseFixed2.filter(x => x['TaskName'] === name)
    this.updateSumTotalHours();
  }

  requirementCount() {
    if (this.loggedInuserDetails.UserType == 'Employee') {
      this.RangeEmployeeID = this.loggedInuserDetails.EmployeeId;
    }

    this.addSubscription = this.timesheetService.searchTasksDateRange("REQCOUNT", this.RangeEmployeeID, null, null).subscribe((response) => {
      if (response && response["successful"]) {
        this.reqc[2] = response['data'][0].bugc;
        this.reqc[1] = response['data'][0].ideac;
        this.reqc[0] = response['data'][0].reqc;
      } else {
      }
    });
  }

  sort() {
    this.getRangeTasks.sort((a, b) => (a['TotalHours'] < b['TotalHours']) ? 1 : -1);
  }

  // end range view


  // start change password

  changePasswordPopUp(input) {
    this.ChangePWDPopUpModal.show();
    this.Title = input;
    this.type = input;
  }

  changePassword(Model, input) {
    this.successmsg = '';
    this.unsuccessmsg = '';
    Model.EmailId = this.loggedInuserDetails.EmailId;
    // Model.NewPassword = this.timesheetService.decrypt(Model.NewPassword);
    this.searchTasksOnInitSubscription = this.timesheetService.login(Model, input).subscribe((response) => {
      if (response && response["successful"]) {
        this.successmsg = response['data'][0]['successmsg'] ? response['data'][0]['successmsg'] : '';
        this.unsuccessmsg = response['data'][0]['unsuccessmsg'] ? response['data'][0]['unsuccessmsg'] : '';
        if (this.successmsg == 'Password is Changed.') {
          this.logout();
        }
      }
      else {
        this.unsuccessmsg = 'Something Went Wrong';
      }
    });
  }

  closeChangePwdPopUpModal() {
    this.ChangePWDPopUpModal.hide();
    this.ChangePwdForm.resetForm();
  }

  // end change password
  // ******* Successfully and unsuccessfully message hide click on screen ********//
  @HostListener('document:click', ['$event'])
  documentClick(event: MouseEvent) {
    this.successmsg = '';
    this.unsuccessmsg = '';
  }

  @HostListener('document:dblclick', ['$event'])
  documentClick1(event: MouseEvent) {
    if (this.PopupNotes.isShown === true) {
      this.PopupNotes.hide();
    }
  }


  //*****unique id corresponding to the item*****//
  trackByFn(item) {
    return item.id;
  }

  //*****unuse the services when component change *****//
  ngOnDestroy() {
    this.searchTasksOnInitSubscription ? this.searchTasksOnInitSubscription.unsubscribe() : null;
    this.searchOnInitSubscription ? this.searchOnInitSubscription.unsubscribe() : null;
  }
}