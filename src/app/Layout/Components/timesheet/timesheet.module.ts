import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TimesheetComponent } from './timesheet.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { MaterialModule } from 'src/app/material-module';
import { TimesheetRoutingModule } from './timesheet-routing.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RequiremntsComponent } from './requiremnts/requiremnts.component';
import { SharedComponentsModule } from 'src/app/Layout/Components/SharedComponents/shared-components/shared-components.module';
import { HolidayComponent } from './holiday/holiday.component';
import { PaySlipComponent } from './pay-slip/pay-slip.component';
 

@NgModule({
  declarations: [TimesheetComponent, RequiremntsComponent, HolidayComponent, PaySlipComponent],
  imports: [
    CommonModule,
    TimesheetRoutingModule,
    FormsModule,
    NgxPaginationModule,
    MaterialModule,
    MDBBootstrapModule,
    SharedComponentsModule
  ]
})
export class TimesheetModule { }
