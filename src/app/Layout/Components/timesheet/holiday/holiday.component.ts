import { Component, OnInit, HostListener, ViewChild, OnDestroy } from '@angular/core';
import { IHoliday } from 'src/app/Layout/Models/timesheet.model';
import { DatePipe } from '@angular/common';
import { TimesheetService } from 'src/app/Layout/Services/timesheet.service';
import { Subscription } from 'rxjs';
import { ModalDirective } from 'angular-bootstrap-md';
import { Moment } from 'moment';
import { MatDatepicker } from '@angular/material';
import { SharedService } from 'src/app/Layout/Services/shared.service';
import { IfStmt } from '@angular/compiler';
@Component({
  selector: 'app-holiday',
  templateUrl: './holiday.component.html',
  styleUrls: ['./holiday.component.scss']
})
export class HolidayComponent implements OnInit, OnDestroy {
  @ViewChild('EmpForm', { static: false }) public EmpForm: any;
  @ViewChild('LeaveRequest', { static: false }) public LeaveRequest: any;
  @ViewChild('PopUpModal', { static: false }) public PopUpModal: ModalDirective;
  @ViewChild('modalConfirmDelete', { static: false }) public modalConfirmDelete: ModalDirective;
  public HolidaySubscription: Subscription;
  public HolidayModel: IHoliday = <IHoliday>{};
  public Empview: boolean = false;
  public LeaveTypeFrom: string[] = ["Full Day", "First Half Day", "Second Half Day"];
  public LeaveTypeTo: string[] = ["Full Day", "First Half Day", "Second Half Day"];
  public StartingDate: Date;
  public EndingDate: Date;
  public TotalDays: any;
  public loggedInuserDetails: any;
  public unsuccessmsg: string;
  public successmsg: string;
  public dateRange: any;
  public LeaveRequests: any;
  public EmpPending: number;
  public AllPending: number;
  public chosenDate_3: Date;
  public AllLeaveRequests: any;
  public FromDate: any;
  public ToDate: any;
  public openLeaves: boolean = false;
  public next: boolean = false;
  public getEmployeesCurMonLeave: any;
  public type: any;
  public HolidayDates: any;
  public title: string;
  public today: Date;
  public changeDate: Date;
  public FromDate2: any;
  public ToDate2: any;
  public DeleteInput: any;
  public changeDate2: Date;
  public MonthlyLeavesTaken: any;
  public MonthlyPrevLeaves: any;
  public MonthLeave: any;
  public getMonthLeaves: any;
  public LeaveEmpName: any;
  public LeavEmployeeId: any;
  public getEmployees: any;
  public AdminAllEmpLeaveBal: any;
  public getEmployeesBirthDays: any;
  getEmpInfo: any;

  constructor(private timesheetService: TimesheetService,
    private datePipe: DatePipe,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.loggedInuserDetails = JSON.parse(localStorage.getItem('currentUser'));
    this.loggedInuserDetails = this.loggedInuserDetails[0];
    this.adminAllEmployeeLeaveBal();
    this.HolidayModel.StartingFromType = this.LeaveTypeFrom[0];
    this.HolidayModel.EndingOnType = this.LeaveTypeTo[0];
    this.chosenDate_3 = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
    this.EmpCurLeaves();
    this.today = new Date();
    this.changeDate = this.today;
    this.changeDate2 = this.today;
    this.FromDate2 = (this.today.getFullYear() >= 2020 ? this.today.getFullYear() : 2020) + '-01-01';
    this.ToDate2 = (this.today.getFullYear() >= 2020 ? this.today.getFullYear() : 2020) + '-12-31';
    this.selectHolidayDates('SelectDates', this.FromDate2, this.ToDate2);
    if (this.loggedInuserDetails.UserType == 'Employee') {
      this.leavesTaken((this.today.getFullYear() >= 2020) ? this.today.getFullYear() : 2020);
      this.previousLeaves((this.today.getFullYear() >= 2020) ? this.today.getFullYear() : 2020);
      this.monthlyLeave((this.today.getFullYear() >= 2020) ? this.today.getFullYear() : 2020);
    }

    this.requestSelect(this.chosenDate_3.toLocaleDateString(), new Date().toLocaleDateString());
    this.FromDate = this.chosenDate_3.toLocaleDateString();
    this.ToDate = new Date().toLocaleDateString();

    this.getAllEmployees();
  }

  // start on init by change

  leaveRequests() {
    this.requestSelect(this.chosenDate_3.toLocaleDateString(), new Date().toLocaleDateString());
    this.requestSelectAll(this.chosenDate_3.toLocaleDateString(), new Date().toLocaleDateString());
  }

  leaveBalance() {
    if (this.loggedInuserDetails.UserType == 'Employee') {
      this.leavesTaken((this.today.getFullYear() >= 2020) ? this.today.getFullYear() : 2020);
    }
    this.previousLeaves((this.changeDate2.getFullYear() >= 2020) ? this.changeDate2.getFullYear() : 2020);
    this.monthlyLeave((this.changeDate2.getFullYear() >= 2020) ? this.changeDate2.getFullYear() : 2020);
    this.getAllEmployees();
    this.adminAllEmployeeLeaveBal();
  }


  selectionChange(a) {
    switch (a.selectedStep.label) {
      case "Birthdays":
        this.empBirthDays();
        break;
      case "Leave Balance":
        this.leaveBalance();
        break;
      case "My Info":
        this.myInfoSelect();
        break;
    }
  }

  //  end on init by change


  // start employee

  applyLeave() {
    this.Empview = true;
  }
  dashboard() {
    this.Empview = false;
  }

  chooseDateFrom = (date) => {
    if (date && date !== '') {
      this.HolidayModel.StartingFromDate = new Date(date.StartingFromDate);
      this.daysDifference(null);
    }
  }

  chooseDateTo = (date) => {
    if (date && date !== '') {
      this.HolidayModel.EndingOnDate = new Date(date.EndingOnDate);
      this.daysDifference(null);
    }
  }

  TypeChange(d) {
    this.daysDifference(d);
  }

  daysDifference(d) {
    if (this.HolidayModel.StartingFromDate && this.HolidayModel.EndingOnDate)

      if (this.datePipe.transform(this.HolidayModel.StartingFromDate.toLocaleDateString(), "yyyy-MM-dd") == this.datePipe.transform(this.HolidayModel.EndingOnDate.toLocaleDateString(), "yyyy-MM-dd")) {
        this.LeaveTypeFrom = ["Full Day", "First Half Day", "Second Half Day"];
        this.LeaveTypeTo = ["Full Day", "First Half Day", "Second Half Day"];
        if (d == 'start')
          this.HolidayModel.EndingOnType = this.HolidayModel.StartingFromType;

        if (d == 'end')
          this.HolidayModel.StartingFromType = this.HolidayModel.EndingOnType;

        if (d == null)
          this.HolidayModel.EndingOnType = this.HolidayModel.StartingFromType;

        if (this.HolidayModel.StartingFromType == 'Full Day') {
          this.HolidayModel.Days = 1;
        } else {
          this.HolidayModel.Days = 0.5;
        }
      }
      else
        if (this.datePipe.transform(this.HolidayModel.StartingFromDate.toLocaleDateString(), "yyyy-MM-dd") < this.datePipe.transform(this.HolidayModel.EndingOnDate.toLocaleDateString(), "yyyy-MM-dd")) {
          this.LeaveTypeFrom = ["Full Day", "Second Half Day"];
          this.LeaveTypeTo = ["Full Day", "First Half Day"];
          this.HolidayModel.Days = (this.HolidayModel.EndingOnDate.getTime() - this.HolidayModel.StartingFromDate.getTime()) / (1000 * 3600 * 24) + 1;

          if (this.HolidayModel.StartingFromType == 'First Half Day' || this.HolidayModel.StartingFromType == 'Second Half Day')
            this.HolidayModel.Days = this.HolidayModel.Days - 0.5;

          if (this.HolidayModel.EndingOnType == 'First Half Day' || this.HolidayModel.EndingOnType == 'Second Half Day')
            this.HolidayModel.Days = this.HolidayModel.Days - 0.5;

          if (this.HolidayModel.EndingOnType == null || this.HolidayModel.EndingOnType == null)
            this.HolidayModel.Days = null;
        }

        else {
          this.HolidayModel.Days = null;
        }
  }

  request(model) {
    if (model && model !== '') {
      model.StartingFromDate = model && model.StartingFromDate ? model.StartingFromDate.toLocaleDateString() : null;
      model.EndingOnDate = model && model.EndingOnDate ? model.EndingOnDate.toLocaleDateString() : null;
      this.HolidaySubscription = this.timesheetService.holidayInsert(model, this.loggedInuserDetails.EmployeeId, null, "LEAVEREQUESTINSERT").subscribe((response) => {
        if (response && response["successful"]) {
          this.successmsg = response['data'][0]['msg'];
          this.LeaveRequest.resetForm();
          this.requestSelect(this.chosenDate_3.toLocaleDateString(), new Date().toLocaleDateString());
        } else {
        }
      });
    }
  }

  deleteRequest(model) {
    this.HolidaySubscription = this.timesheetService.holidayInsert(model, this.loggedInuserDetails.EmployeeId, null, "DELETEREQUEST").subscribe((response) => {
      if (response && response["successful"]) {
        this.modalConfirmDelete.hide();
        this.requestSelect(this.chosenDate_3.toLocaleDateString(), new Date().toLocaleDateString());
      } else {
      }
    });
  }


  dateRangeEvent = ($event) => {
    this.dateRange = $event;
    if (this.dateRange.startDate && this.dateRange.endDate) {
      this.requestSelect(this.dateRange.startDate._d.toISOString(), this.dateRange.endDate._d.toISOString())
    }
  }

  requestSelect(FromDate, ToDate) {
    this.HolidaySubscription = this.timesheetService.holidaySelect(this.datePipe.transform(FromDate, "yyyy-MM-dd"), this.datePipe.transform(ToDate, "yyyy-MM-dd"), this.loggedInuserDetails.EmployeeId, "LeaveRequestsSelect", null).subscribe((response) => {
      if (response && response["successful"]) {
        this.LeaveRequests = response['data'][0];
        this.EmpPending = response['data'][1][0]['count'];
      } else {
      }
    });
  }

  // end employee


  // Start Admin
  dateRangeEventAdmin($event) {
    this.dateRange = $event;
    if (this.dateRange.startDate && this.dateRange.endDate) {
      this.requestSelectAll(this.dateRange.startDate._d.toISOString(), this.dateRange.endDate._d.toISOString())
    }

  }

  requestSelectAll(FromDate, ToDate) {
    this.FromDate = FromDate;
    this.ToDate = ToDate;
    this.HolidaySubscription = this.timesheetService.holidaySelect(this.datePipe.transform(FromDate, "yyyy-MM-dd"), this.datePipe.transform(ToDate, "yyyy-MM-dd"), this.loggedInuserDetails.EmployeeId, "AllLeaveRequestsSelect", null).subscribe((response) => {
      if (response && response["successful"]) {
        this.AllLeaveRequests = response['data'][0];
        this.AllLeaveRequests.forEach((element, Index) => {
          let PrevLeaves = this.AdminAllEmpLeaveBal.filter(x => x['EmployeeId'] === element.EmployeeId)[0]['PreviousLeaves'] ? this.AdminAllEmpLeaveBal.filter(x => x['EmployeeId'] === element.EmployeeId)[0]['PreviousLeaves'] : 0;
          let LeavesTaken = this.AdminAllEmpLeaveBal.filter(x => x['EmployeeId'] === element.EmployeeId)[0]['LeavesTaken'] ? this.AdminAllEmpLeaveBal.filter(x => x['EmployeeId'] === element.EmployeeId)[0]['LeavesTaken'] : 0;
          element['Bal'] = PrevLeaves - LeavesTaken;
        });
        this.AllPending = response['data'][1][0]['count'];
        this.sharedService.sharedNotification(this.AllPending);
      }
    });
  }

  status(model, input) {
    this.HolidaySubscription = this.timesheetService.holidayInsert(model, null, null, input).subscribe((response) => {
      if (response && response["successful"]) {
        this.adminAllEmployeeLeaveBal2();
      }
    });
  }

  adminAllEmployeeLeaveBal2() {
    this.HolidaySubscription = this.timesheetService.holidaySelect(null, null, null, "AdminAllEmpLeaveBal", null).subscribe((response) => {
      if (response && response["successful"]) {
        this.AdminAllEmpLeaveBal = response['data'][0];
        this.requestSelectAll(this.FromDate, this.ToDate);
      }
    });
  }

  // end Admin


  // start settings


  EmpCurLeaves = () => {
    this.HolidaySubscription = this.timesheetService.holidaySelect(null, null, null, 'CurMonthLeave', null).subscribe((response) => {
      if (response && response["successful"]) {
        this.getEmployeesCurMonLeave = response['data'][0];
      } else {
      }
    });
  }

  openEmpLeaves(model) {
    this.openLeaves = true;
    this.LeaveEmpName = model.EmployeeName;
    this.HolidayModel.EmployeeId = model.EmployeeId;
    this.monthLeavesByEmpId(model.EmployeeId);
  }

  closeEmpLeaves() {
    this.openLeaves = false;
    this.LeaveEmpName = null;
  }

  monthLeavesByEmpId(EmployeeId) {
    this.HolidaySubscription = this.timesheetService.holidaySelect(null, null, EmployeeId, 'MonthLeave', null).subscribe((response) => {
      if (response && response["successful"]) {
        this.getMonthLeaves = response['data'][0];
      } else {
      }
    });
  }

  insertEmployeeLeaves(HolidayModel, input) {
    this.HolidaySubscription = this.timesheetService.holidayInsert(HolidayModel, null, null, input).subscribe((response) => {
      if (response && response["successful"]) {
        this.successmsg = response['data'][0]['msg'];
        this.EmpForm.resetForm();
        this.EmpCurLeaves();
        this.monthLeavesByEmpId(HolidayModel.EmployeeId);
      }
    });
  }


  chosenMonthHandlerFrom(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    datepicker.close();
    this.HolidayModel.StartingFromDate = new Date(this.datePipe.transform(normalizedMonth.toString(), "yyyy-MM-dd"))
  }

  chosenMonthHandlerTo(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    datepicker.close();
    let dt = new Date(this.datePipe.transform(normalizedMonth.toString(), "yyyy-MM-dd"))
    this.HolidayModel.EndingOnDate = new Date(dt.getFullYear(), dt.getMonth() + 1, 0);
  }

  deleteEmpHoliday(HolidayModel, input) {
    this.HolidaySubscription = this.timesheetService.holidayInsert(HolidayModel, null, null, input).subscribe((response) => {
      if (response && response["successful"]) {
        this.monthLeavesByEmpId(HolidayModel.EmployeeId);
        this.EmpCurLeaves();
        this.modalConfirmDelete.hide();
      }
    });
  }

  insertNewEmployee(HolidayModel, input) {
    this.HolidaySubscription = this.timesheetService.holidayInsert(HolidayModel, null, this.datePipe.transform(this.HolidayModel.Date.toLocaleDateString(), "yyyy-MM-dd"), input).subscribe((response) => {
      if (response && response["successful"]) {
        this.HolidayModel.EmpId = String(
          this.getEmployeesCurMonLeave.map((e) => { return Number(e.EmpId) }).reduce((a, b) => { return (a > b ? a : b) }) + 2);
        this.EmpCurLeaves();
        this.successmsg = response['data'][0]['msg'];
        this.HolidayModel.EmployeeName = '';
        this.HolidayModel.MailId = '';
        this.HolidayModel.Designation = 'Software Engineer R&D';
        this.HolidayModel.Department = 'Software R&D';
        this.HolidayModel.Date = null;
        if (this.HolidayModel.EmpId.length == 1) {
          this.HolidayModel.EmpId = '00' + this.HolidayModel.EmpId;
        }
        if (this.HolidayModel.EmpId.length == 2) {
          this.HolidayModel.EmpId = '0' + this.HolidayModel.EmpId;
        }
      }
    });
  }


  editNewEmployee(HolidayModel, input) {
    this.HolidaySubscription = this.timesheetService.holidayInsert(HolidayModel, null, this.datePipe.transform(this.HolidayModel.Date.toLocaleDateString(), "yyyy-MM-dd"), input).subscribe((response) => {
      if (response && response["successful"]) {
        this.EmpCurLeaves();
        this.successmsg = response['data'][0]['msg'];
        this.EmpForm.resetForm();
      }
    });
  }

  // end settings

  // start holiday

  previousYear() {
    if ((this.changeDate.getFullYear() - 1) >= 2020) {
      this.changeDate = new Date(this.changeDate.setFullYear(this.changeDate.getFullYear() - 1));
      this.FromDate2 = this.changeDate.getFullYear() + '-01-01';
      this.ToDate2 = this.changeDate.getFullYear() + '-12-31';
      this.selectHolidayDates('SelectDates', this.FromDate2, this.ToDate2);
    }
  }

  nextYear() {

    this.changeDate = new Date(this.changeDate.setFullYear(this.changeDate.getFullYear() + 1));
    this.FromDate2 = this.changeDate.getFullYear() + '-01-01';
    this.ToDate2 = this.changeDate.getFullYear() + '-12-31';
    this.selectHolidayDates('SelectDates', this.FromDate2, this.ToDate2);

  }

  insertHolidayDates(HolidayModel, input) {
    this.HolidaySubscription = this.timesheetService.holidayInsert(HolidayModel, null, this.datePipe.transform(this.HolidayModel.Date.toLocaleDateString(), "yyyy-MM-dd"), input).subscribe((response) => {
      if (response && response["successful"]) {
        this.successmsg = response['data'][0]['msg'];
        this.EmpForm.resetForm();
        this.selectHolidayDates('SelectDates', this.FromDate2, this.ToDate2);
      }
    });
  }

  selectHolidayDates(input, FromDate2, ToDate2) {
    this.HolidaySubscription = this.timesheetService.holidaySelect(FromDate2, ToDate2, null, input, null).subscribe((response) => {
      if (response && response["successful"]) {
        this.HolidayDates = response['data'][0];
      }
    });
  }

  deleteHolidayDates(HolidayModel, input) {
    this.HolidaySubscription = this.timesheetService.holidayInsert(HolidayModel, null, null, input).subscribe((response) => {
      if (response && response["successful"]) {
        this.successmsg = response['data'][0]['msg'];
        this.selectHolidayDates('SelectDates', this.FromDate2, this.ToDate2);
        this.modalConfirmDelete.hide();
      }
    });
  }

  //  end holiday

  //start leave balance

  previousYear2() {
    if ((this.changeDate2.getFullYear() - 1) >= 2020) {
      this.changeDate2 = new Date(this.changeDate2.setFullYear(this.changeDate2.getFullYear() - 1));
      this.leavesTaken(this.changeDate2.getFullYear());
      this.previousLeaves(this.changeDate2.getFullYear());
      this.monthlyLeave(this.changeDate2.getFullYear());
    }
  }

  nextYear2() {
    if ((this.changeDate2.getFullYear() + 1) <= this.today.getFullYear()) {
      this.changeDate2 = new Date(this.changeDate2.setFullYear(this.changeDate2.getFullYear() + 1));
      this.leavesTaken(this.changeDate2.getFullYear());
      this.previousLeaves(this.changeDate2.getFullYear());
      this.monthlyLeave(this.changeDate2.getFullYear());
    }
  }

  leavesTaken(Year) {
    if (this.loggedInuserDetails.UserType == 'Employee') {
      this.LeavEmployeeId = this.loggedInuserDetails.EmployeeId;
    }
    this.HolidaySubscription = this.timesheetService.holidaySelect(null, null, this.LeavEmployeeId, "MonthlyLeavesTaken", Year).subscribe((response) => {
      if (response && response["successful"]) {
        this.MonthlyLeavesTaken = response['data'][0][0];
      }
    });
  }
  adminAllEmployeeLeaveBal() {
    this.HolidaySubscription = this.timesheetService.holidaySelect(null, null, null, "AdminAllEmpLeaveBal", null).subscribe((response) => {
      if (response && response["successful"]) {
        this.AdminAllEmpLeaveBal = response['data'][0];
        this.requestSelectAll(this.FromDate, this.ToDate);
      }
    });
  }


  previousLeaves(Year) {
    if (this.loggedInuserDetails.UserType == 'Employee') {
      this.LeavEmployeeId = this.loggedInuserDetails.EmployeeId;
    }
    this.HolidaySubscription = this.timesheetService.holidaySelect(null, null, this.LeavEmployeeId, "MonthlyPrevLeaves", Year).subscribe((response) => {
      if (response && response["successful"]) {
        this.MonthlyPrevLeaves = response['data'][0][0];
      }
    });
  }

  monthlyLeave(Year) {
    if (this.loggedInuserDetails.UserType == 'Employee') {
      this.LeavEmployeeId = this.loggedInuserDetails.EmployeeId;
    }
    this.HolidaySubscription = this.timesheetService.holidaySelect(null, null, this.LeavEmployeeId, "MonthLeavebyMonth", Year).subscribe((response) => {
      if (response && response["successful"]) {
        this.MonthLeave = response['data'][0][0];
      }
    });
  }

  DataByEmp(EmployeeId) {
    this.HolidayModel.EmployeeId = EmployeeId;
    this.LeavEmployeeId = EmployeeId;
    this.leavesTaken((this.today.getFullYear() >= 2020) ? this.today.getFullYear() : 2020);
    this.previousLeaves((this.today.getFullYear() >= 2020) ? this.today.getFullYear() : 2020);
    this.monthlyLeave((this.today.getFullYear() >= 2020) ? this.today.getFullYear() : 2020);
  }

  previousEmployee() {
    if (this.next) {
      let index = this.getEmployees.findIndex(x => x['EmployeeId'] === this.LeavEmployeeId)
      if (index != 0) {
        let EmpId = this.getEmployees[index - 1]['EmployeeId'];
        if (EmpId > 2) {
          this.DataByEmp(EmpId);
        }
      }
    }
  }

  nextEmployee() {
    let index = this.getEmployees.findIndex(x => x['EmployeeId'] === this.LeavEmployeeId)
    if (index != this.getEmployees.length - 1) {
      this.next = true;
      let EmpId = this.getEmployees[index + 1]['EmployeeId'];
      this.DataByEmp(EmpId);
    }
  }
  getAllEmployees() {
    this.HolidaySubscription = this.timesheetService.holidaySelect(null, null, null, "AllEmployees", null).subscribe((response) => {
      if (response && response["successful"]) {
        this.getEmployees = response['data'][0];
      }
    });
  }


  //end leave balance

  // start birthDay

  empBirthDays() {
    this.HolidaySubscription = this.timesheetService.holidaySelect(null, null, null, "BirthDay", null).subscribe((response) => {
      if (response && response["successful"]) {
        this.getEmployeesBirthDays = response['data'][0];
      }
    });
  }

  birthDayInsert(HolidayModel, model) {
    HolidayModel.EmployeeId = model.EmployeeId;
    this.HolidayModel.Date.setFullYear(2000);
    this.HolidaySubscription = this.timesheetService.holidayInsert(HolidayModel, null, this.datePipe.transform(this.HolidayModel.Date.toLocaleDateString(), "yyyy-MM-dd"), "BirthDayInsert").subscribe((response) => {
      if (response && response["successful"]) {
        this.empBirthDays();
      }
    });
  }

  //end Birthday

  // start my info

  myInfoSelect() {
    this.HolidaySubscription = this.timesheetService.holidaySelect(null, null, this.loggedInuserDetails.EmployeeId, "EmployeeInfo", null).subscribe((response) => {
      if (response && response["successful"]) {
        this.getEmpInfo = response['data'][0][0];
      }
    });
  }

  // end my info

  // start common
  createPopup = (model, input) => {

    switch (input) {
      case "UpdateLeaves":
        this.HolidayModel.MonthlyLeaves = model.MonthlyLeaves;
        this.HolidayModel.EmployeeName = model.EmployeeName;
        this.HolidayModel.EmployeeId = model.EmployeeId;
        this.title = '';
        break;

      case "InsertDates":
        this.title = 'Add Holiday';
        break;


      case "Add MonthltLeaveRange":
        this.title = 'Add Monthly Leave Range';
        break;


      case "Edit Employee":
        this.HolidayModel.EmployeeName = model.EmployeeName;
        this.HolidayModel.Id = model.Id;
        this.HolidayModel.EmpId = model.EmpId;
        this.HolidayModel.MailId = model.EmailId;
        this.HolidayModel.Designation = model.Designation;
        this.HolidayModel.Department = model.Department;
        this.HolidayModel.EmployeeName = model.EmployeeName;
        this.HolidayModel.EmployeeId = model.EmployeeId;
        this.HolidayModel.Date = new Date(model.JoiningDate);
        if (model.JoiningDate == null) {
          this.HolidayModel.Date = null;
        }
        this.title = 'Update Employee Details';
        break;

      case "Add Employee":
        this.title = 'Add Employee';
        this.HolidayModel.Designation = 'Software Engineer R&D';
        this.HolidayModel.Department = 'Software R&D';
        this.HolidayModel.EmpId = String(
          this.getEmployeesCurMonLeave.map((e) => {
            return Number(e.EmpId)
          }).reduce((a, b) => { return (a > b ? a : b) }
          ) + 1
        );
        if (this.HolidayModel.EmpId.length == 1) {
          this.HolidayModel.EmpId = '00' + this.HolidayModel.EmpId;
        }
        if (this.HolidayModel.EmpId.length == 2) {
          this.HolidayModel.EmpId = '0' + this.HolidayModel.EmpId;
        }
        break;

    }

    this.type = input;
    this.PopUpModal.show();
  }

  closePopUpModal() {
    this.EmpForm.resetForm();
    this.PopUpModal.hide();
    this.successmsg = "";
    this.unsuccessmsg = "";
  }

  deletePopup = (model, input) => {
    this.DeleteInput = input;
    this.modalConfirmDelete.show();
    if (input == "DELETEHolidayDate") {
      this.HolidayModel.Id = model.Id;
    }
    if (input == "DELETEREQUEST") {
      this.HolidayModel.LeaveId = model.LeaveId;
    }
    if (input == "DeleteEmployeeLeaves") {
      this.HolidayModel.Id = model.Id;
    }
  }
  // end common


  @HostListener('document:click', ['$event'])
  documentClick(event: MouseEvent) {
    this.successmsg = '';
    this.unsuccessmsg = '';
  }
  trackByFn(item) {
    return item.id;
  }

  ngOnDestroy() {
    this.HolidaySubscription ? this.HolidaySubscription.unsubscribe() : null;
  }
}
