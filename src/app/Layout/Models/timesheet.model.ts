export interface IAddModel {
    Status: string,
    ProjectId: number,
    ProjectName: string,
    PhaseId: number,
    PhaseName: string,
    TaskId: number,
    TaskName: string,
    EmployeeId2: number;
    EmployeeId: number,
    EmployeeName: string,
    Hours: number;
    DailyTaskId: number;
    DailyTaskNotes: string;
    Date: Date;
    startdate: Date;
    FromDate: Date;
    ToDate: Date;
    RangeEmployeeID: number;
    month: string;
}

export interface ILogin {
    EmailId: string,
    Password: string,
    NewPassword: string,
    VerifyNewPassword: string
}

export interface IRequirement {
    Date: string,
    Type: string,
    ProjectName: string,
    PhaseName: string,
    ProjectId: number,
    PhaseId: number,
    TaskName: string,
    RequirementName: string,
    RequirementDetail: string,
    From: string,
    To: String,
    Priority: string,
    Effort: number,
    Status: string,
    TaskId: number,
    Id: number
}


export interface IHoliday {
    StartingFromDate: Date,
    EndingOnDate: Date,
    StartingFromType: string,
    EndingOnType: string
    Days: number,
    Remark: string,
    Status: string,
    ApproverNotes: string,
    EmployeeName: string,
    MonthlyLeaves: number,
    EmployeeId: number,
    Date: Date,
    Event: string,
    Type: string,
    Id: number,
    LeaveId: number,
    EmpId: string,
    Designation: string,
    Department: string,
    MailId: string
}

export interface IPaySlip {
    Month?: Date,
    month: Date,
    EmployeeId: number,
    EmployeeName: string,
    Gross: number,
    Deduction: number,
    LossOfPay: number,
    Allowance: number,
    Net: number,
    Remark: string,
    Status: string
}