import { Injectable } from '@angular/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { FileAttachment } from '../Components/SharedComponents/file-upload/file-attachment.model';
import { HttpClient } from '@angular/common/http';
import { TimesheetService } from 'src/app/Layout/Services/timesheet.service';
@Injectable({
providedIn: 'root'
})
export class PayslipPdfService {
public Data: any;
public FileAttachment: any;
constructor(private timesheetService: TimesheetService,
private http: HttpClient) { }
toDataURL(url, callback) {
var xhr = new XMLHttpRequest();
xhr.onload = function () {
var reader = new FileReader();
reader.onloadend = function () {
callback(reader.result);
}
reader.readAsDataURL(xhr.response);
};
xhr.open('GET', url);
xhr.responseType = 'blob';
xhr.send();
}

CreatePdf = (LeadData) => {
let promise = new Promise((resolve) => {
this.toDataURL(`https://truimages.blob.core.windows.net/productionimages/Document/Email/51d32e19-3a68-442d-bbed-b9074b98484cCordisoletterpad.png`, function (dataUrl) {
// var doc = new jsPDF();
var doc = new jsPDF("p", "mm", "a4");

var width = doc.internal.pageSize.getWidth();
var height = doc.internal.pageSize.getHeight();
doc.addImage(dataUrl, 'png', 0, 0, width, height, 'Logo');
doc.setFontSize(11);
// APPLICANT //

doc.fromHTML('<b>Payslip Reference  ' + `${LeadData.PayslipReference ? LeadData.PayslipReference : ''}` + '</b>', 10, 60); //59
// doc.roundedRect(9, 70, 193, 72, 4, 4, 'S');
doc.roundedRect(9, 70, 193, 64, 4, 4, 'S');
doc.fromHTML('<b>Pay Slip for ' + `${LeadData.Month ? LeadData.Month : ''}` + ` ${LeadData.Year ? LeadData.Year : ''}` + '</b>', 80, 69); //59
doc.line(9, 78, 202, 78);
// doc.line(100, 78, 100, 142);
doc.line(100, 78, 100, 134);
doc.setFontStyle('Georgia');
doc.setFontSize(10);
// Employee Id

doc.fromHTML('<b>Employee Id </b>', 25, 76);
doc.fromHTML('<b>' + `${LeadData.EmpId ? LeadData.EmpId : ''}` + '</b>', 125, 76);
doc.line(9, 85, 202, 85);

doc.fromHTML('<b>Employee Name</b>', 25, 83);
doc.fromHTML('<b>' + `${LeadData.EmployeeName ? LeadData.EmployeeName : ''}` + '</b>', 125, 83);
doc.line(9, 92, 202, 92);

doc.fromHTML('<b>Designation</b>', 25, 90);
doc.fromHTML('<b>' + `${LeadData.Designation ? LeadData.Designation : ''}` + '</b>', 125, 90);
doc.line(9, 99, 202, 99);

doc.fromHTML('<b>Department </b>', 25, 97);
doc.fromHTML('<b>' + `${LeadData.Department ? LeadData.Department : ''}` + '</b>', 125, 97);
doc.line(9, 106, 202, 106);

doc.fromHTML('<b>Gross Salary </b>', 25, 104);
doc.fromHTML('<b>' + `${LeadData.GrossSalary ? LeadData.GrossSalary : ''}` + '</b>', 125, 104);
doc.line(9, 112, 202, 112);


doc.fromHTML('<b>Allowance</b>', 25, 111);
doc.fromHTML('<b>' + `${LeadData.Allowance ? LeadData.Allowance : ''}` + '</b>', 125, 111);
doc.line(9, 119, 202, 119);


doc.fromHTML('<b>Deduction </b>', 25, 118);
doc.fromHTML('<b>' + `${LeadData.Deduction ? LeadData.Deduction : ''}` + '</b>', 125, 118);
doc.line(9, 126, 202, 126);


doc.fromHTML('<b>Net Salary</b>', 25, 125);
doc.fromHTML('<b>' + `${LeadData.NetSalary ? LeadData.NetSalary : ''}` + '</b>', 125, 125);
doc.fromHTML('<b> * Variable bonus is applicable after 1 year of permanent service with company. </b>', 25, 135);



// doc.fromHTML('<b>Loss of Pay </b>', 25, 125);
// doc.fromHTML('<b>' + `${LeadData.LossOfPay ? LeadData.LossOfPay : ''}` + '</b>', 125, 125);
// doc.line(9, 133, 202, 133);
// // doc.addPage();
// doc.fromHTML('<b>Net Salary</b>', 25, 132);
// doc.fromHTML('<b>' + `${LeadData.NetSalary ? LeadData.NetSalary : ''}` + '</b>', 125, 132);
// doc.fromHTML('<b> * Variable bonus is applicable after 1 year of permanent service with company. </b>', 25, 145);


// pdf.output('payslip.pdf');
var xyz = doc.output('blob');
let CreatedFile = [];
const reader: FileReader = new FileReader();
reader.readAsDataURL(xyz);
reader.onload = (e) => {
const fileAttachment: FileAttachment = new FileAttachment();
let fileContent: any = reader.result;
fileContent = fileContent.split('base64,')[1];
fileAttachment.content = fileContent;
fileAttachment.name = `${new Date().getTime()}_${LeadData.EmpId}_payslip_${LeadData.Month}.pdf`;
fileAttachment.type = xyz.type;
fileAttachment.size = xyz.size;
fileAttachment.documentName = 'payslip';
fileAttachment.metadata = [];
CreatedFile.push(fileAttachment);
resolve(CreatedFile);
}
});
});
return promise;
}
}