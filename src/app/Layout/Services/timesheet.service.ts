import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class TimesheetService {

  constructor(private http: HttpClient) { }

  searchOnInit = (input) => {
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetSelect/UserId,EntityId,UserType?Input=${input}`;
    return this.http.get(url, this.jwt()).pipe(map(x => x));
  }

  searchTasksOnInit = (EntityId, input, TaskDate) => {
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetSelect/UserId,EntityId,UserType?Input=${input}&EmployeeId=${EntityId}&TaskDate=${TaskDate}`;
    return this.http.get(url, this.jwt()).pipe(map(x => x));
  }

  searchTasksDateRange = (input, EntityId, FromDate, ToDate) => {
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetSelect/UserId,EntityId,UserType?Input=${input}&EmployeeId=${EntityId}&FromDate=${FromDate}&ToDate=${ToDate}`;
    return this.http.get(url, this.jwt()).pipe(map(x => x));
  }

  mouseOverRange = (input, model) => {
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetSelect/UserId,EntityId,UserType?Input=${input}&TaskId=${model.TaskId}&PhaseId=${model.PhaseId}`;
    return this.http.get(url, this.jwt()).pipe(map(x => x));
  }

  add = (EmployeeId, Model, Input, Date) => {
    let data = {
      Status: Model.Status ? Model.Status : null,
      ProjectId: Model.ProjectId ? Model.ProjectId : null,
      ProjectName: Model.ProjectName ? Model.ProjectName : null,
      PhaseId: Model.PhaseId ? Model.PhaseId : null,
      PhaseName: Model.PhaseName ? Model.PhaseName : null,
      TaskId: Model.TaskId ? Model.TaskId : null,
      TaskName: Model.TaskName ? Model.TaskName : null,
      Hours: Model.Hours ? Model.Hours : null,
      DailyTaskId: Model.DailyTaskId ? Model.DailyTaskId : null,
      DailyTaskNotes: Model.DailyTaskNotes ? Model.DailyTaskNotes : null,
      EmployeeId: EmployeeId ? EmployeeId : null,
      Date: Date ? Date : null,
      Input: Input ? Input : null
    }
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetInsertUpdateDelete/UserId,EntityId,UserType?`;
    return this.http.post(url, data, this.jwt()).pipe(map(x => x), take(1));
  }

  login = (model, Input) => {
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetLogin/UserId,EntityId,UserType?EmailId=${model.EmailId}&Password=${model.Password}&NewPassword=${model.NewPassword}&Input=${Input}`;
    return this.http.get(url, this.jwt()).pipe(map(x => x));
  }
  // start requirement
  saveRequirement = (Model, Input) => {
    let data = {
      Date: Model.Date ? Model.Date : null,
      ProjectId: Model.ProjectId ? Model.ProjectId : null,
      ProjectName: Model.ProjectName ? Model.ProjectName : null,
      PhaseId: Model.PhaseId ? Model.PhaseId : null,
      PhaseName: Model.PhaseName ? Model.PhaseName : null,
      RequirementName: Model.RequirementName ? Model.RequirementName : null,
      RequirementDetail: Model.RequirementDetail ? Model.RequirementDetail : null,
      Type: Model.Type ? Model.Type : null,
      From: Model.From ? Model.From : null,
      To: Model.To ? Model.To : null,
      Priority: Model.Priority ? Model.Priority : null,
      Effort: Model.Effort ? Model.Effort : null,
      Status: Model.Status ? Model.Status : null,
      Id: Model.Id ? Model.Id : null,
      TaskId: Model.TaskId ? Model.TaskId : null,
      Input: Input ? Input : null
    }
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetRequirementInsertion/UserId,EntityId,UserType`;
    return this.http.post(url, data, this.jwt()).pipe(map(x => x), take(1));
  }

  onInitRequirements(Input, Id) {
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetRequirementSelection/UserId,EntityId,UserType?Input=${Input}&Id=${Id}`;
    return this.http.get(url, this.jwt()).pipe(map(x => x));
  }

  // end requirement

  // start Holiday

  holidayInsert = (Model, EmployeeId, Date, Input) => {
    let data = {
      StartingFromDate: Model.StartingFromDate ? Model.StartingFromDate : null,
      EndingOnDate: Model.EndingOnDate ? Model.EndingOnDate : null,
      StartingFromType: Model.StartingFromType ? Model.StartingFromType : null,
      EndingOnType: Model.EndingOnType ? Model.EndingOnType : null,
      Days: Model.Days ? Model.Days : null,
      Remark: Model.Remark ? Model.Remark : null,
      Status: Model.Status ? Model.Status : null,
      ApproverNotes: Model.ApproverNotes ? Model.ApproverNotes : null,
      EmployeeId_t: Model.EmployeeId ? Model.EmployeeId : null,
      MonthlyLeaves: Model.MonthlyLeaves ? Model.MonthlyLeaves : null,
      LeaveId: Model.LeaveId ? Model.LeaveId : null,
      Id: Model.Id ? Model.Id : null,
      Event: Model.Event ? Model.Event : null,
      EmployeeId: EmployeeId ? EmployeeId : null,
      EmpId: Model.EmpId ? Model.EmpId : null,
      EmployeeName: Model.EmployeeName ? Model.EmployeeName : null,
      Department: Model.Department ? Model.Department : null,
      Designation: Model.Designation ? Model.Designation : null,
      MailId: Model.MailId ? Model.MailId : null,
      Date: Date ? Date : null,
      Input: Input ? Input : null
    }
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetHolidayInsertion/UserId,EntityId,UserType`;
    return this.http.post(url, data, this.jwt()).pipe(map(x => x), take(1));
  }


  holidaySelect(FromDate, ToDate, EmployeeId, Input, Year) {
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetHolidaySelection/UserId,EntityId,UserType?Input=${Input}&FromDate=${FromDate}&ToDate=${ToDate}&EmployeeId=${EmployeeId}&Year=${Year}`;
    return this.http.get(url, this.jwt()).pipe(map(x => x));
  }


  // end Holiday


  // start Payslip

  onInitPaySlip(Input, Month, EmployeeId) {
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetPaySlipSelection/UserId,EntityId,UserType?Input=${Input}&Month=${Month}&EmployeeId=${EmployeeId}`;
    return this.http.get(url, this.jwt()).pipe(map(x => x));
  }

  savePaySlip(Model, Input) {
    let data = {
      EmployeeId: Model.EmployeeId ? Model.EmployeeId : null,
      Gross: Model.Gross ? Model.Gross : null,
      Deduction: Model.Deduction ? Model.Deduction : null,
      LossOfPay: Model.LossOfPay ? Model.LossOfPay : null,
      Allowance: Model.Allowance ? Model.Allowance : null,
      Net: Model.Net ? Model.Net : null,
      Remark: Model.Remark ? Model.Remark : null,
      Month: Model.Month ? Model.Month : null,
      Status: Model.Status ? Model.Status : null,
      Id: Model.Id ? Model.Id : null,
      Input: Input ? Input : null
    }
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetPaySlipInsertion/UserId,EntityId,UserType`;
    return this.http.post(url, data, this.jwt()).pipe(map(x => x), take(1));
  }

  importPayslipDetails = (uploadExcelFile, Month) => {
    let data = {
      Month: Month,
      files: uploadExcelFile
    }
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/ITTimeSheetPaySlipExcelImport`;
    return this.http.post(url, JSON.stringify(data), this.jwt()).pipe(map(x => x), take(1));
  }


  sevice(Month, EmployeeId, fileAttachment, UserId, Input) {
    let data = {
      sendmailmodel: {
        Month: Month,
        employeeId: EmployeeId,
        UserId: UserId,
        input: Input
      },
      File: fileAttachment
    }
    const url = `${environment['hrApiUrl']}api/ITTimeSheet/SendMailPaySlip/UserId,EntityId,UserType`;
    return this.http.post(url, JSON.stringify(data), this.jwt()).pipe(map(x => x), take(1));
  }

  // end Payslip

  public jwt() {
    if (localStorage.getItem('currentUser') && JSON.parse(localStorage.getItem('currentUser'))) {
      return {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token
        })
      }
    }
  }

}
