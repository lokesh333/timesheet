import { TestBed } from '@angular/core/testing';

import { PayslipPdfService } from './payslip-pdf.service';

describe('PayslipPdfService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PayslipPdfService = TestBed.get(PayslipPdfService);
    expect(service).toBeTruthy();
  });
});
